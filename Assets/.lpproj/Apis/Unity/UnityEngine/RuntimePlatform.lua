---@class UnityEngine.RuntimePlatform : System.Enum
UnityEngine.RuntimePlatform = {}

---@field public UnityEngine.RuntimePlatform.value__ : System.Int32
UnityEngine.RuntimePlatform.value__ = nil

---@field public UnityEngine.RuntimePlatform.OSXEditor : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.OSXEditor = nil

---@field public UnityEngine.RuntimePlatform.OSXPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.OSXPlayer = nil

---@field public UnityEngine.RuntimePlatform.WindowsPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WindowsPlayer = nil

---@field public UnityEngine.RuntimePlatform.OSXWebPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.OSXWebPlayer = nil

---@field public UnityEngine.RuntimePlatform.OSXDashboardPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.OSXDashboardPlayer = nil

---@field public UnityEngine.RuntimePlatform.WindowsWebPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WindowsWebPlayer = nil

---@field public UnityEngine.RuntimePlatform.WindowsEditor : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WindowsEditor = nil

---@field public UnityEngine.RuntimePlatform.IPhonePlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.IPhonePlayer = nil

---@field public UnityEngine.RuntimePlatform.XBOX360 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.XBOX360 = nil

---@field public UnityEngine.RuntimePlatform.PS3 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.PS3 = nil

---@field public UnityEngine.RuntimePlatform.Android : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.Android = nil

---@field public UnityEngine.RuntimePlatform.NaCl : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.NaCl = nil

---@field public UnityEngine.RuntimePlatform.FlashPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.FlashPlayer = nil

---@field public UnityEngine.RuntimePlatform.LinuxPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.LinuxPlayer = nil

---@field public UnityEngine.RuntimePlatform.LinuxEditor : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.LinuxEditor = nil

---@field public UnityEngine.RuntimePlatform.WebGLPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WebGLPlayer = nil

---@field public UnityEngine.RuntimePlatform.MetroPlayerX86 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.MetroPlayerX86 = nil

---@field public UnityEngine.RuntimePlatform.WSAPlayerX86 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WSAPlayerX86 = nil

---@field public UnityEngine.RuntimePlatform.MetroPlayerX64 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.MetroPlayerX64 = nil

---@field public UnityEngine.RuntimePlatform.WSAPlayerX64 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WSAPlayerX64 = nil

---@field public UnityEngine.RuntimePlatform.MetroPlayerARM : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.MetroPlayerARM = nil

---@field public UnityEngine.RuntimePlatform.WSAPlayerARM : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WSAPlayerARM = nil

---@field public UnityEngine.RuntimePlatform.WP8Player : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WP8Player = nil

---@field public UnityEngine.RuntimePlatform.BB10Player : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.BB10Player = nil

---@field public UnityEngine.RuntimePlatform.BlackBerryPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.BlackBerryPlayer = nil

---@field public UnityEngine.RuntimePlatform.TizenPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.TizenPlayer = nil

---@field public UnityEngine.RuntimePlatform.PSP2 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.PSP2 = nil

---@field public UnityEngine.RuntimePlatform.PS4 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.PS4 = nil

---@field public UnityEngine.RuntimePlatform.PSM : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.PSM = nil

---@field public UnityEngine.RuntimePlatform.XboxOne : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.XboxOne = nil

---@field public UnityEngine.RuntimePlatform.SamsungTVPlayer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.SamsungTVPlayer = nil

---@field public UnityEngine.RuntimePlatform.WiiU : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WiiU = nil

---@field public UnityEngine.RuntimePlatform.tvOS : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.tvOS = nil

---@field public UnityEngine.RuntimePlatform.Switch : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.Switch = nil

---@field public UnityEngine.RuntimePlatform.Lumin : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.Lumin = nil

---@field public UnityEngine.RuntimePlatform.Stadia : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.Stadia = nil

---@field public UnityEngine.RuntimePlatform.CloudRendering : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.CloudRendering = nil

---@field public UnityEngine.RuntimePlatform.GameCoreScarlett : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.GameCoreScarlett = nil

---@field public UnityEngine.RuntimePlatform.GameCoreXboxSeries : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.GameCoreXboxSeries = nil

---@field public UnityEngine.RuntimePlatform.GameCoreXboxOne : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.GameCoreXboxOne = nil

---@field public UnityEngine.RuntimePlatform.PS5 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.PS5 = nil

---@field public UnityEngine.RuntimePlatform.EmbeddedLinuxArm64 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.EmbeddedLinuxArm64 = nil

---@field public UnityEngine.RuntimePlatform.EmbeddedLinuxArm32 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.EmbeddedLinuxArm32 = nil

---@field public UnityEngine.RuntimePlatform.EmbeddedLinuxX64 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.EmbeddedLinuxX64 = nil

---@field public UnityEngine.RuntimePlatform.EmbeddedLinuxX86 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.EmbeddedLinuxX86 = nil

---@field public UnityEngine.RuntimePlatform.LinuxServer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.LinuxServer = nil

---@field public UnityEngine.RuntimePlatform.WindowsServer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.WindowsServer = nil

---@field public UnityEngine.RuntimePlatform.OSXServer : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.OSXServer = nil

---@field public UnityEngine.RuntimePlatform.QNXArm32 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.QNXArm32 = nil

---@field public UnityEngine.RuntimePlatform.QNXArm64 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.QNXArm64 = nil

---@field public UnityEngine.RuntimePlatform.QNXX64 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.QNXX64 = nil

---@field public UnityEngine.RuntimePlatform.QNXX86 : UnityEngine.RuntimePlatform
UnityEngine.RuntimePlatform.QNXX86 = nil