---@class UnityEngine.FieldOfViewAxis : System.Enum
UnityEngine.FieldOfViewAxis = {}

---@field public UnityEngine.FieldOfViewAxis.value__ : System.Int32
UnityEngine.FieldOfViewAxis.value__ = nil

---@field public UnityEngine.FieldOfViewAxis.Vertical : UnityEngine.FieldOfViewAxis
UnityEngine.FieldOfViewAxis.Vertical = nil

---@field public UnityEngine.FieldOfViewAxis.Horizontal : UnityEngine.FieldOfViewAxis
UnityEngine.FieldOfViewAxis.Horizontal = nil