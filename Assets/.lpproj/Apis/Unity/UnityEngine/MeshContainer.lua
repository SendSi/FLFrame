---@class UnityEngine.MeshContainer : System.ValueType
UnityEngine.MeshContainer = {}

---@field public UnityEngine.MeshContainer.gameObject : UnityEngine.GameObject
UnityEngine.MeshContainer.gameObject = nil

---@field public UnityEngine.MeshContainer.instance : UnityEngine.MeshInstance
UnityEngine.MeshContainer.instance = nil

---@field public UnityEngine.MeshContainer.subMeshInstances : System.Collections.Generic.List
UnityEngine.MeshContainer.subMeshInstances = nil