---@class UnityEngine.CustomRenderTextureManager : System.Object
UnityEngine.CustomRenderTextureManager = {}

---@param value : System.Action
function UnityEngine.CustomRenderTextureManager.add_textureLoaded(value)
end

---@param value : System.Action
function UnityEngine.CustomRenderTextureManager.remove_textureLoaded(value)
end

---@param value : System.Action
function UnityEngine.CustomRenderTextureManager.add_textureUnloaded(value)
end

---@param value : System.Action
function UnityEngine.CustomRenderTextureManager.remove_textureUnloaded(value)
end

---@param currentCustomRenderTextures : System.Collections.Generic.List
function UnityEngine.CustomRenderTextureManager.GetAllCustomRenderTextures(currentCustomRenderTextures)
end

---@param value : System.Action
function UnityEngine.CustomRenderTextureManager.add_updateTriggered(value)
end

---@param value : System.Action
function UnityEngine.CustomRenderTextureManager.remove_updateTriggered(value)
end

---@param value : System.Action
function UnityEngine.CustomRenderTextureManager.add_initializeTriggered(value)
end

---@param value : System.Action
function UnityEngine.CustomRenderTextureManager.remove_initializeTriggered(value)
end