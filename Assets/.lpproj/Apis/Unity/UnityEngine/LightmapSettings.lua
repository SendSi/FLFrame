---@class UnityEngine.LightmapSettings : UnityEngine.Object
UnityEngine.LightmapSettings = {}

---@property readwrite UnityEngine.LightmapSettings.lightmaps : UnityEngine.LightmapData[]
UnityEngine.LightmapSettings.lightmaps = nil

---@property readwrite UnityEngine.LightmapSettings.lightmapsMode : UnityEngine.LightmapsMode
UnityEngine.LightmapSettings.lightmapsMode = nil

---@property readwrite UnityEngine.LightmapSettings.lightProbes : UnityEngine.LightProbes
UnityEngine.LightmapSettings.lightProbes = nil

---@property readwrite UnityEngine.LightmapSettings.lightmapsModeLegacy : UnityEngine.LightmapsModeLegacy
UnityEngine.LightmapSettings.lightmapsModeLegacy = nil

---@property readwrite UnityEngine.LightmapSettings.bakedColorSpace : UnityEngine.ColorSpace
UnityEngine.LightmapSettings.bakedColorSpace = nil