---@class UnityEngine.JointDrive : System.ValueType
UnityEngine.JointDrive = {}

---@property readwrite UnityEngine.JointDrive.positionSpring : System.Single
UnityEngine.JointDrive.positionSpring = nil

---@property readwrite UnityEngine.JointDrive.positionDamper : System.Single
UnityEngine.JointDrive.positionDamper = nil

---@property readwrite UnityEngine.JointDrive.maximumForce : System.Single
UnityEngine.JointDrive.maximumForce = nil

---@property readwrite UnityEngine.JointDrive.mode : UnityEngine.JointDriveMode
UnityEngine.JointDrive.mode = nil