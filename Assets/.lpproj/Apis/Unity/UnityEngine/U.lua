---@class UnityEngine.U : System.ValueType
UnityEngine.U = {}

---@field public UnityEngine.U.p8 : System.BytePointer
UnityEngine.U.p8 = nil

---@field public UnityEngine.U.p32 : System.UInt32Pointer
UnityEngine.U.p32 = nil

---@field public UnityEngine.U.p64 : System.UInt64Pointer
UnityEngine.U.p64 = nil

---@field public UnityEngine.U.i : System.UInt64
UnityEngine.U.i = nil

---@param p8 : System.UInt16Pointer
---@return UnityEngine.U
function UnityEngine.U(p8)
end