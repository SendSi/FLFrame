---@class UnityEngine.SceneManagement.LoadSceneMode : System.Enum
UnityEngine.SceneManagement.LoadSceneMode = {}

---@field public UnityEngine.SceneManagement.LoadSceneMode.value__ : System.Int32
UnityEngine.SceneManagement.LoadSceneMode.value__ = nil

---@field public UnityEngine.SceneManagement.LoadSceneMode.Single : UnityEngine.SceneManagement.LoadSceneMode
UnityEngine.SceneManagement.LoadSceneMode.Single = nil

---@field public UnityEngine.SceneManagement.LoadSceneMode.Additive : UnityEngine.SceneManagement.LoadSceneMode
UnityEngine.SceneManagement.LoadSceneMode.Additive = nil