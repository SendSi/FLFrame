---@class UnityEngine.StackTraceLogType : System.Enum
UnityEngine.StackTraceLogType = {}

---@field public UnityEngine.StackTraceLogType.value__ : System.Int32
UnityEngine.StackTraceLogType.value__ = nil

---@field public UnityEngine.StackTraceLogType.None : UnityEngine.StackTraceLogType
UnityEngine.StackTraceLogType.None = nil

---@field public UnityEngine.StackTraceLogType.ScriptOnly : UnityEngine.StackTraceLogType
UnityEngine.StackTraceLogType.ScriptOnly = nil

---@field public UnityEngine.StackTraceLogType.Full : UnityEngine.StackTraceLogType
UnityEngine.StackTraceLogType.Full = nil