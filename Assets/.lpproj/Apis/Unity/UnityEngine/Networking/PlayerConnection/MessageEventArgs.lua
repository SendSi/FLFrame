---@class UnityEngine.Networking.PlayerConnection.MessageEventArgs : System.Object
UnityEngine.Networking.PlayerConnection.MessageEventArgs = {}

---@field public UnityEngine.Networking.PlayerConnection.MessageEventArgs.playerId : System.Int32
UnityEngine.Networking.PlayerConnection.MessageEventArgs.playerId = nil

---@field public UnityEngine.Networking.PlayerConnection.MessageEventArgs.data : System.Byte[]
UnityEngine.Networking.PlayerConnection.MessageEventArgs.data = nil

---@return UnityEngine.Networking.PlayerConnection.MessageEventArgs
function UnityEngine.Networking.PlayerConnection.MessageEventArgs()
end