---@class UnityEngine.DepthTextureMode : System.Enum
UnityEngine.DepthTextureMode = {}

---@field public UnityEngine.DepthTextureMode.value__ : System.Int32
UnityEngine.DepthTextureMode.value__ = nil

---@field public UnityEngine.DepthTextureMode.None : UnityEngine.DepthTextureMode
UnityEngine.DepthTextureMode.None = nil

---@field public UnityEngine.DepthTextureMode.Depth : UnityEngine.DepthTextureMode
UnityEngine.DepthTextureMode.Depth = nil

---@field public UnityEngine.DepthTextureMode.DepthNormals : UnityEngine.DepthTextureMode
UnityEngine.DepthTextureMode.DepthNormals = nil

---@field public UnityEngine.DepthTextureMode.MotionVectors : UnityEngine.DepthTextureMode
UnityEngine.DepthTextureMode.MotionVectors = nil