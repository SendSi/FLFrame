---@class UnityEngine.ISerializationCallbackReceiver
UnityEngine.ISerializationCallbackReceiver = {}

function UnityEngine.ISerializationCallbackReceiver:OnBeforeSerialize()
end

function UnityEngine.ISerializationCallbackReceiver:OnAfterDeserialize()
end