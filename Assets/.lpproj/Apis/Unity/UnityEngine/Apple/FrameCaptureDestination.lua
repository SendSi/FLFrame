---@module UnityEngine.Apple
UnityEngine.Apple = {}

---@class UnityEngine.Apple.FrameCaptureDestination : System.Enum
UnityEngine.Apple.FrameCaptureDestination = {}

---@field public UnityEngine.Apple.FrameCaptureDestination.value__ : System.Int32
UnityEngine.Apple.FrameCaptureDestination.value__ = nil

---@field public UnityEngine.Apple.FrameCaptureDestination.DevTools : UnityEngine.Apple.FrameCaptureDestination
UnityEngine.Apple.FrameCaptureDestination.DevTools = nil

---@field public UnityEngine.Apple.FrameCaptureDestination.GPUTraceDocument : UnityEngine.Apple.FrameCaptureDestination
UnityEngine.Apple.FrameCaptureDestination.GPUTraceDocument = nil