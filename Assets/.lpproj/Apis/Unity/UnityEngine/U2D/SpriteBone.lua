---@class UnityEngine.U2D.SpriteBone : System.ValueType
UnityEngine.U2D.SpriteBone = {}

---@property readwrite UnityEngine.U2D.SpriteBone.name : System.String
UnityEngine.U2D.SpriteBone.name = nil

---@property readwrite UnityEngine.U2D.SpriteBone.guid : System.String
UnityEngine.U2D.SpriteBone.guid = nil

---@property readwrite UnityEngine.U2D.SpriteBone.position : UnityEngine.Vector3
UnityEngine.U2D.SpriteBone.position = nil

---@property readwrite UnityEngine.U2D.SpriteBone.rotation : UnityEngine.Quaternion
UnityEngine.U2D.SpriteBone.rotation = nil

---@property readwrite UnityEngine.U2D.SpriteBone.length : System.Single
UnityEngine.U2D.SpriteBone.length = nil

---@property readwrite UnityEngine.U2D.SpriteBone.parentId : System.Int32
UnityEngine.U2D.SpriteBone.parentId = nil

---@property readwrite UnityEngine.U2D.SpriteBone.color : UnityEngine.Color32
UnityEngine.U2D.SpriteBone.color = nil