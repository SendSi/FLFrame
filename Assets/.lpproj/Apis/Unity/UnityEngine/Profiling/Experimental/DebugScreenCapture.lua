---@module UnityEngine.Profiling.Experimental
UnityEngine.Profiling.Experimental = {}

---@class UnityEngine.Profiling.Experimental.DebugScreenCapture : System.ValueType
UnityEngine.Profiling.Experimental.DebugScreenCapture = {}

---@property readwrite UnityEngine.Profiling.Experimental.DebugScreenCapture.rawImageDataReference : Unity.Collections.NativeArray
UnityEngine.Profiling.Experimental.DebugScreenCapture.rawImageDataReference = nil

---@property readwrite UnityEngine.Profiling.Experimental.DebugScreenCapture.imageFormat : UnityEngine.TextureFormat
UnityEngine.Profiling.Experimental.DebugScreenCapture.imageFormat = nil

---@property readwrite UnityEngine.Profiling.Experimental.DebugScreenCapture.width : System.Int32
UnityEngine.Profiling.Experimental.DebugScreenCapture.width = nil

---@property readwrite UnityEngine.Profiling.Experimental.DebugScreenCapture.height : System.Int32
UnityEngine.Profiling.Experimental.DebugScreenCapture.height = nil