---@class UnityEngine.ArticulationDofLock : System.Enum
UnityEngine.ArticulationDofLock = {}

---@field public UnityEngine.ArticulationDofLock.value__ : System.Int32
UnityEngine.ArticulationDofLock.value__ = nil

---@field public UnityEngine.ArticulationDofLock.LockedMotion : UnityEngine.ArticulationDofLock
UnityEngine.ArticulationDofLock.LockedMotion = nil

---@field public UnityEngine.ArticulationDofLock.LimitedMotion : UnityEngine.ArticulationDofLock
UnityEngine.ArticulationDofLock.LimitedMotion = nil

---@field public UnityEngine.ArticulationDofLock.FreeMotion : UnityEngine.ArticulationDofLock
UnityEngine.ArticulationDofLock.FreeMotion = nil