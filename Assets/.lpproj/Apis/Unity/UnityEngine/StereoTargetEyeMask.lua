---@class UnityEngine.StereoTargetEyeMask : System.Enum
UnityEngine.StereoTargetEyeMask = {}

---@field public UnityEngine.StereoTargetEyeMask.value__ : System.Int32
UnityEngine.StereoTargetEyeMask.value__ = nil

---@field public UnityEngine.StereoTargetEyeMask.None : UnityEngine.StereoTargetEyeMask
UnityEngine.StereoTargetEyeMask.None = nil

---@field public UnityEngine.StereoTargetEyeMask.Left : UnityEngine.StereoTargetEyeMask
UnityEngine.StereoTargetEyeMask.Left = nil

---@field public UnityEngine.StereoTargetEyeMask.Right : UnityEngine.StereoTargetEyeMask
UnityEngine.StereoTargetEyeMask.Right = nil

---@field public UnityEngine.StereoTargetEyeMask.Both : UnityEngine.StereoTargetEyeMask
UnityEngine.StereoTargetEyeMask.Both = nil