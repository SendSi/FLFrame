---@class UnityEngine.FrameTiming : System.ValueType
UnityEngine.FrameTiming = {}

---@field public UnityEngine.FrameTiming.cpuFrameTime : System.Double
UnityEngine.FrameTiming.cpuFrameTime = nil

---@field public UnityEngine.FrameTiming.cpuMainThreadFrameTime : System.Double
UnityEngine.FrameTiming.cpuMainThreadFrameTime = nil

---@field public UnityEngine.FrameTiming.cpuMainThreadPresentWaitTime : System.Double
UnityEngine.FrameTiming.cpuMainThreadPresentWaitTime = nil

---@field public UnityEngine.FrameTiming.cpuRenderThreadFrameTime : System.Double
UnityEngine.FrameTiming.cpuRenderThreadFrameTime = nil

---@field public UnityEngine.FrameTiming.gpuFrameTime : System.Double
UnityEngine.FrameTiming.gpuFrameTime = nil

---@field public UnityEngine.FrameTiming.frameStartTimestamp : System.UInt64
UnityEngine.FrameTiming.frameStartTimestamp = nil

---@field public UnityEngine.FrameTiming.firstSubmitTimestamp : System.UInt64
UnityEngine.FrameTiming.firstSubmitTimestamp = nil

---@field public UnityEngine.FrameTiming.cpuTimePresentCalled : System.UInt64
UnityEngine.FrameTiming.cpuTimePresentCalled = nil

---@field public UnityEngine.FrameTiming.cpuTimeFrameComplete : System.UInt64
UnityEngine.FrameTiming.cpuTimeFrameComplete = nil

---@field public UnityEngine.FrameTiming.heightScale : System.Single
UnityEngine.FrameTiming.heightScale = nil

---@field public UnityEngine.FrameTiming.widthScale : System.Single
UnityEngine.FrameTiming.widthScale = nil

---@field public UnityEngine.FrameTiming.syncInterval : System.UInt32
UnityEngine.FrameTiming.syncInterval = nil