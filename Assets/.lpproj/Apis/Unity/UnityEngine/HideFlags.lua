---@class UnityEngine.HideFlags : System.Enum
UnityEngine.HideFlags = {}

---@field public UnityEngine.HideFlags.value__ : System.Int32
UnityEngine.HideFlags.value__ = nil

---@field public UnityEngine.HideFlags.None : UnityEngine.HideFlags
UnityEngine.HideFlags.None = nil

---@field public UnityEngine.HideFlags.HideInHierarchy : UnityEngine.HideFlags
UnityEngine.HideFlags.HideInHierarchy = nil

---@field public UnityEngine.HideFlags.HideInInspector : UnityEngine.HideFlags
UnityEngine.HideFlags.HideInInspector = nil

---@field public UnityEngine.HideFlags.DontSaveInEditor : UnityEngine.HideFlags
UnityEngine.HideFlags.DontSaveInEditor = nil

---@field public UnityEngine.HideFlags.NotEditable : UnityEngine.HideFlags
UnityEngine.HideFlags.NotEditable = nil

---@field public UnityEngine.HideFlags.DontSaveInBuild : UnityEngine.HideFlags
UnityEngine.HideFlags.DontSaveInBuild = nil

---@field public UnityEngine.HideFlags.DontUnloadUnusedAsset : UnityEngine.HideFlags
UnityEngine.HideFlags.DontUnloadUnusedAsset = nil

---@field public UnityEngine.HideFlags.DontSave : UnityEngine.HideFlags
UnityEngine.HideFlags.DontSave = nil

---@field public UnityEngine.HideFlags.HideAndDontSave : UnityEngine.HideFlags
UnityEngine.HideFlags.HideAndDontSave = nil