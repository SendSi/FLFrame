---@class UnityEngine.RenderTextureCreationFlags : System.Enum
UnityEngine.RenderTextureCreationFlags = {}

---@field public UnityEngine.RenderTextureCreationFlags.value__ : System.Int32
UnityEngine.RenderTextureCreationFlags.value__ = nil

---@field public UnityEngine.RenderTextureCreationFlags.MipMap : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.MipMap = nil

---@field public UnityEngine.RenderTextureCreationFlags.AutoGenerateMips : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.AutoGenerateMips = nil

---@field public UnityEngine.RenderTextureCreationFlags.SRGB : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.SRGB = nil

---@field public UnityEngine.RenderTextureCreationFlags.EyeTexture : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.EyeTexture = nil

---@field public UnityEngine.RenderTextureCreationFlags.EnableRandomWrite : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.EnableRandomWrite = nil

---@field public UnityEngine.RenderTextureCreationFlags.CreatedFromScript : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.CreatedFromScript = nil

---@field public UnityEngine.RenderTextureCreationFlags.AllowVerticalFlip : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.AllowVerticalFlip = nil

---@field public UnityEngine.RenderTextureCreationFlags.NoResolvedColorSurface : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.NoResolvedColorSurface = nil

---@field public UnityEngine.RenderTextureCreationFlags.DynamicallyScalable : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.DynamicallyScalable = nil

---@field public UnityEngine.RenderTextureCreationFlags.BindMS : UnityEngine.RenderTextureCreationFlags
UnityEngine.RenderTextureCreationFlags.BindMS = nil