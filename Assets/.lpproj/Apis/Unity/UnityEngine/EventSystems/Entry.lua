---@class UnityEngine.EventSystems.Entry : System.Object
UnityEngine.EventSystems.Entry = {}

---@field public UnityEngine.EventSystems.Entry.eventID : UnityEngine.EventSystems.EventTriggerType
UnityEngine.EventSystems.Entry.eventID = nil

---@field public UnityEngine.EventSystems.Entry.callback : UnityEngine.EventSystems.TriggerEvent
UnityEngine.EventSystems.Entry.callback = nil

---@return UnityEngine.EventSystems.Entry
function UnityEngine.EventSystems.Entry()
end