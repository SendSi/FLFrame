---@class UnityEngine.EventSystems.InputButton : System.Enum
UnityEngine.EventSystems.InputButton = {}

---@field public UnityEngine.EventSystems.InputButton.value__ : System.Int32
UnityEngine.EventSystems.InputButton.value__ = nil

---@field public UnityEngine.EventSystems.InputButton.Left : UnityEngine.EventSystems.InputButton
UnityEngine.EventSystems.InputButton.Left = nil

---@field public UnityEngine.EventSystems.InputButton.Right : UnityEngine.EventSystems.InputButton
UnityEngine.EventSystems.InputButton.Right = nil

---@field public UnityEngine.EventSystems.InputButton.Middle : UnityEngine.EventSystems.InputButton
UnityEngine.EventSystems.InputButton.Middle = nil