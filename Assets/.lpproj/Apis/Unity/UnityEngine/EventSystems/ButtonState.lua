---@class UnityEngine.EventSystems.ButtonState : System.Object
UnityEngine.EventSystems.ButtonState = {}

---@property readwrite UnityEngine.EventSystems.ButtonState.eventData : UnityEngine.EventSystems.MouseButtonEventData
UnityEngine.EventSystems.ButtonState.eventData = nil

---@property readwrite UnityEngine.EventSystems.ButtonState.button : UnityEngine.EventSystems.InputButton
UnityEngine.EventSystems.ButtonState.button = nil

---@return UnityEngine.EventSystems.ButtonState
function UnityEngine.EventSystems.ButtonState()
end