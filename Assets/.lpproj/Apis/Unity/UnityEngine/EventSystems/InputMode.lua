---@class UnityEngine.EventSystems.InputMode : System.Enum
UnityEngine.EventSystems.InputMode = {}

---@field public UnityEngine.EventSystems.InputMode.value__ : System.Int32
UnityEngine.EventSystems.InputMode.value__ = nil

---@field public UnityEngine.EventSystems.InputMode.Mouse : UnityEngine.EventSystems.InputMode
UnityEngine.EventSystems.InputMode.Mouse = nil

---@field public UnityEngine.EventSystems.InputMode.Buttons : UnityEngine.EventSystems.InputMode
UnityEngine.EventSystems.InputMode.Buttons = nil