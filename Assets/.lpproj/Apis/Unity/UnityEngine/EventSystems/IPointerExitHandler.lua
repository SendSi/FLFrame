---@class UnityEngine.EventSystems.IPointerExitHandler
UnityEngine.EventSystems.IPointerExitHandler = {}

---@param eventData : UnityEngine.EventSystems.PointerEventData
function UnityEngine.EventSystems.IPointerExitHandler:OnPointerExit(eventData)
end