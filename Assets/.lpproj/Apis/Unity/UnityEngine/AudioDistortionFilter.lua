---@class UnityEngine.AudioDistortionFilter : UnityEngine.Behaviour
UnityEngine.AudioDistortionFilter = {}

---@property readwrite UnityEngine.AudioDistortionFilter.distortionLevel : System.Single
UnityEngine.AudioDistortionFilter.distortionLevel = nil

---@return UnityEngine.AudioDistortionFilter
function UnityEngine.AudioDistortionFilter()
end