---@class UnityEngine.QualityLevel : System.Enum
UnityEngine.QualityLevel = {}

---@field public UnityEngine.QualityLevel.value__ : System.Int32
UnityEngine.QualityLevel.value__ = nil

---@field public UnityEngine.QualityLevel.Fastest : UnityEngine.QualityLevel
UnityEngine.QualityLevel.Fastest = nil

---@field public UnityEngine.QualityLevel.Fast : UnityEngine.QualityLevel
UnityEngine.QualityLevel.Fast = nil

---@field public UnityEngine.QualityLevel.Simple : UnityEngine.QualityLevel
UnityEngine.QualityLevel.Simple = nil

---@field public UnityEngine.QualityLevel.Good : UnityEngine.QualityLevel
UnityEngine.QualityLevel.Good = nil

---@field public UnityEngine.QualityLevel.Beautiful : UnityEngine.QualityLevel
UnityEngine.QualityLevel.Beautiful = nil

---@field public UnityEngine.QualityLevel.Fantastic : UnityEngine.QualityLevel
UnityEngine.QualityLevel.Fantastic = nil