---@class UnityEngine.DisableBatchingType : System.Enum
UnityEngine.DisableBatchingType = {}

---@field public UnityEngine.DisableBatchingType.value__ : System.Int32
UnityEngine.DisableBatchingType.value__ = nil

---@field public UnityEngine.DisableBatchingType.False : UnityEngine.DisableBatchingType
UnityEngine.DisableBatchingType.False = nil

---@field public UnityEngine.DisableBatchingType.True : UnityEngine.DisableBatchingType
UnityEngine.DisableBatchingType.True = nil

---@field public UnityEngine.DisableBatchingType.WhenLODFading : UnityEngine.DisableBatchingType
UnityEngine.DisableBatchingType.WhenLODFading = nil