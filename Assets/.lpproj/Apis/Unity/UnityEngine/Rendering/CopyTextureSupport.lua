---@class UnityEngine.Rendering.CopyTextureSupport : System.Enum
UnityEngine.Rendering.CopyTextureSupport = {}

---@field public UnityEngine.Rendering.CopyTextureSupport.value__ : System.Int32
UnityEngine.Rendering.CopyTextureSupport.value__ = nil

---@field public UnityEngine.Rendering.CopyTextureSupport.None : UnityEngine.Rendering.CopyTextureSupport
UnityEngine.Rendering.CopyTextureSupport.None = nil

---@field public UnityEngine.Rendering.CopyTextureSupport.Basic : UnityEngine.Rendering.CopyTextureSupport
UnityEngine.Rendering.CopyTextureSupport.Basic = nil

---@field public UnityEngine.Rendering.CopyTextureSupport.Copy3D : UnityEngine.Rendering.CopyTextureSupport
UnityEngine.Rendering.CopyTextureSupport.Copy3D = nil

---@field public UnityEngine.Rendering.CopyTextureSupport.DifferentTypes : UnityEngine.Rendering.CopyTextureSupport
UnityEngine.Rendering.CopyTextureSupport.DifferentTypes = nil

---@field public UnityEngine.Rendering.CopyTextureSupport.TextureToRT : UnityEngine.Rendering.CopyTextureSupport
UnityEngine.Rendering.CopyTextureSupport.TextureToRT = nil

---@field public UnityEngine.Rendering.CopyTextureSupport.RTToTexture : UnityEngine.Rendering.CopyTextureSupport
UnityEngine.Rendering.CopyTextureSupport.RTToTexture = nil