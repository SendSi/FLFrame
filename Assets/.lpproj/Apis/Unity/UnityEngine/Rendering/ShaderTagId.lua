---@class UnityEngine.Rendering.ShaderTagId : System.ValueType
UnityEngine.Rendering.ShaderTagId = {}

---@field public UnityEngine.Rendering.ShaderTagId.none : UnityEngine.Rendering.ShaderTagId
UnityEngine.Rendering.ShaderTagId.none = nil

---@property readonly UnityEngine.Rendering.ShaderTagId.name : System.String
UnityEngine.Rendering.ShaderTagId.name = nil

---@param name : System.String
---@return UnityEngine.Rendering.ShaderTagId
function UnityEngine.Rendering.ShaderTagId(name)
end

---@param obj : System.Object
---@return System.Boolean
function UnityEngine.Rendering.ShaderTagId:Equals(obj)
end

---@param other : UnityEngine.Rendering.ShaderTagId
---@return System.Boolean
function UnityEngine.Rendering.ShaderTagId:Equals(other)
end

---@return System.Int32
function UnityEngine.Rendering.ShaderTagId:GetHashCode()
end

---@param tag1 : UnityEngine.Rendering.ShaderTagId
---@param tag2 : UnityEngine.Rendering.ShaderTagId
---@return System.Boolean
function UnityEngine.Rendering.ShaderTagId.op_Equality(tag1, tag2)
end

---@param tag1 : UnityEngine.Rendering.ShaderTagId
---@param tag2 : UnityEngine.Rendering.ShaderTagId
---@return System.Boolean
function UnityEngine.Rendering.ShaderTagId.op_Inequality(tag1, tag2)
end

---@param name : System.String
---@return UnityEngine.Rendering.ShaderTagId
function UnityEngine.Rendering.ShaderTagId.op_Explicit(name)
end

---@param tagId : UnityEngine.Rendering.ShaderTagId
---@return System.String
function UnityEngine.Rendering.ShaderTagId.op_Explicit(tagId)
end