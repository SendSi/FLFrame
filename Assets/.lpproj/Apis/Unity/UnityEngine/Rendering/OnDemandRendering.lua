---@class UnityEngine.Rendering.OnDemandRendering : System.Object
UnityEngine.Rendering.OnDemandRendering = {}

---@property readonly UnityEngine.Rendering.OnDemandRendering.willCurrentFrameRender : System.Boolean
UnityEngine.Rendering.OnDemandRendering.willCurrentFrameRender = nil

---@property readwrite UnityEngine.Rendering.OnDemandRendering.renderFrameInterval : System.Int32
UnityEngine.Rendering.OnDemandRendering.renderFrameInterval = nil

---@property readonly UnityEngine.Rendering.OnDemandRendering.effectiveRenderFrameRate : System.Int32
UnityEngine.Rendering.OnDemandRendering.effectiveRenderFrameRate = nil

---@return UnityEngine.Rendering.OnDemandRendering
function UnityEngine.Rendering.OnDemandRendering()
end