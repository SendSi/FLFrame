---@class UnityEngine.Rendering.AmbientMode : System.Enum
UnityEngine.Rendering.AmbientMode = {}

---@field public UnityEngine.Rendering.AmbientMode.value__ : System.Int32
UnityEngine.Rendering.AmbientMode.value__ = nil

---@field public UnityEngine.Rendering.AmbientMode.Skybox : UnityEngine.Rendering.AmbientMode
UnityEngine.Rendering.AmbientMode.Skybox = nil

---@field public UnityEngine.Rendering.AmbientMode.Trilight : UnityEngine.Rendering.AmbientMode
UnityEngine.Rendering.AmbientMode.Trilight = nil

---@field public UnityEngine.Rendering.AmbientMode.Flat : UnityEngine.Rendering.AmbientMode
UnityEngine.Rendering.AmbientMode.Flat = nil

---@field public UnityEngine.Rendering.AmbientMode.Custom : UnityEngine.Rendering.AmbientMode
UnityEngine.Rendering.AmbientMode.Custom = nil