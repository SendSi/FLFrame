---@class UnityEngine.Rendering.BuiltinRenderTextureType : System.Enum
UnityEngine.Rendering.BuiltinRenderTextureType = {}

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.value__ : System.Int32
UnityEngine.Rendering.BuiltinRenderTextureType.value__ = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.PropertyName : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.PropertyName = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.BufferPtr : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.BufferPtr = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.RenderTexture : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.RenderTexture = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.BindableTexture : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.BindableTexture = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.None : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.None = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.CurrentActive : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.CurrentActive = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.CameraTarget : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.CameraTarget = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.Depth : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.Depth = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.DepthNormals : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.DepthNormals = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.ResolvedDepth : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.ResolvedDepth = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.PrepassNormalsSpec : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.PrepassNormalsSpec = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.PrepassLight : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.PrepassLight = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.PrepassLightSpec : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.PrepassLightSpec = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer0 : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer0 = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer1 : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer1 = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer2 : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer2 = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer3 : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer3 = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.Reflections : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.Reflections = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.MotionVectors : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.MotionVectors = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer4 : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer4 = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer5 : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer5 = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer6 : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer6 = nil

---@field public UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer7 : UnityEngine.Rendering.BuiltinRenderTextureType
UnityEngine.Rendering.BuiltinRenderTextureType.GBuffer7 = nil