---@class UnityEngine.Rendering.RenderQueue : System.Enum
UnityEngine.Rendering.RenderQueue = {}

---@field public UnityEngine.Rendering.RenderQueue.value__ : System.Int32
UnityEngine.Rendering.RenderQueue.value__ = nil

---@field public UnityEngine.Rendering.RenderQueue.Background : UnityEngine.Rendering.RenderQueue
UnityEngine.Rendering.RenderQueue.Background = nil

---@field public UnityEngine.Rendering.RenderQueue.Geometry : UnityEngine.Rendering.RenderQueue
UnityEngine.Rendering.RenderQueue.Geometry = nil

---@field public UnityEngine.Rendering.RenderQueue.AlphaTest : UnityEngine.Rendering.RenderQueue
UnityEngine.Rendering.RenderQueue.AlphaTest = nil

---@field public UnityEngine.Rendering.RenderQueue.GeometryLast : UnityEngine.Rendering.RenderQueue
UnityEngine.Rendering.RenderQueue.GeometryLast = nil

---@field public UnityEngine.Rendering.RenderQueue.Transparent : UnityEngine.Rendering.RenderQueue
UnityEngine.Rendering.RenderQueue.Transparent = nil

---@field public UnityEngine.Rendering.RenderQueue.Overlay : UnityEngine.Rendering.RenderQueue
UnityEngine.Rendering.RenderQueue.Overlay = nil