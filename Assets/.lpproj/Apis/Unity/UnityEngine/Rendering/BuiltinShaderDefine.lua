---@class UnityEngine.Rendering.BuiltinShaderDefine : System.Enum
UnityEngine.Rendering.BuiltinShaderDefine = {}

---@field public UnityEngine.Rendering.BuiltinShaderDefine.value__ : System.Int32
UnityEngine.Rendering.BuiltinShaderDefine.value__ = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_DXT5nm : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_DXT5nm = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_RGBM : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_RGBM = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_USE_NATIVE_HDR : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_USE_NATIVE_HDR = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_ENABLE_REFLECTION_BUFFERS : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_ENABLE_REFLECTION_BUFFERS = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_FRAMEBUFFER_FETCH_AVAILABLE : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_FRAMEBUFFER_FETCH_AVAILABLE = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_ENABLE_NATIVE_SHADOW_LOOKUPS : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_ENABLE_NATIVE_SHADOW_LOOKUPS = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_METAL_SHADOWS_USE_POINT_FILTERING : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_METAL_SHADOWS_USE_POINT_FILTERING = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_CUBEMAP_ARRAY : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_CUBEMAP_ARRAY = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_SCREENSPACE_SHADOWS : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_SCREENSPACE_SHADOWS = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_USE_DITHER_MASK_FOR_ALPHABLENDED_SHADOWS : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_USE_DITHER_MASK_FOR_ALPHABLENDED_SHADOWS = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_PBS_USE_BRDF1 : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_PBS_USE_BRDF1 = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_PBS_USE_BRDF2 : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_PBS_USE_BRDF2 = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_PBS_USE_BRDF3 : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_PBS_USE_BRDF3 = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_FULL_STANDARD_SHADER : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_NO_FULL_STANDARD_SHADER = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_SPECCUBE_BOX_PROJECTION : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_SPECCUBE_BOX_PROJECTION = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_SPECCUBE_BLENDING : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_SPECCUBE_BLENDING = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_ENABLE_DETAIL_NORMALMAP : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_ENABLE_DETAIL_NORMALMAP = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.SHADER_API_MOBILE : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.SHADER_API_MOBILE = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.SHADER_API_DESKTOP : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.SHADER_API_DESKTOP = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_HARDWARE_TIER1 : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_HARDWARE_TIER1 = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_HARDWARE_TIER2 : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_HARDWARE_TIER2 = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_HARDWARE_TIER3 : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_HARDWARE_TIER3 = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_COLORSPACE_GAMMA : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_COLORSPACE_GAMMA = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_LIGHT_PROBE_PROXY_VOLUME : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_LIGHT_PROBE_PROXY_VOLUME = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_HALF_PRECISION_FRAGMENT_SHADER_REGISTERS : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_HALF_PRECISION_FRAGMENT_SHADER_REGISTERS = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_LIGHTMAP_DLDR_ENCODING : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_LIGHTMAP_DLDR_ENCODING = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_LIGHTMAP_RGBM_ENCODING : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_LIGHTMAP_RGBM_ENCODING = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_LIGHTMAP_FULL_HDR : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_LIGHTMAP_FULL_HDR = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_VIRTUAL_TEXTURING : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_VIRTUAL_TEXTURING = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_PRETRANSFORM_TO_DISPLAY_ORIENTATION : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_PRETRANSFORM_TO_DISPLAY_ORIENTATION = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_ASTC_NORMALMAP_ENCODING : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_ASTC_NORMALMAP_ENCODING = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.SHADER_API_GLES30 : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.SHADER_API_GLES30 = nil

---@field public UnityEngine.Rendering.BuiltinShaderDefine.UNITY_UNIFIED_SHADER_PRECISION_MODEL : UnityEngine.Rendering.BuiltinShaderDefine
UnityEngine.Rendering.BuiltinShaderDefine.UNITY_UNIFIED_SHADER_PRECISION_MODEL = nil