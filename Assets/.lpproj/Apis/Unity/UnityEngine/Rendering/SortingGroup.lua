---@class UnityEngine.Rendering.SortingGroup : UnityEngine.Behaviour
UnityEngine.Rendering.SortingGroup = {}

---@property readwrite UnityEngine.Rendering.SortingGroup.sortingLayerName : System.String
UnityEngine.Rendering.SortingGroup.sortingLayerName = nil

---@property readwrite UnityEngine.Rendering.SortingGroup.sortingLayerID : System.Int32
UnityEngine.Rendering.SortingGroup.sortingLayerID = nil

---@property readwrite UnityEngine.Rendering.SortingGroup.sortingOrder : System.Int32
UnityEngine.Rendering.SortingGroup.sortingOrder = nil

---@property readwrite UnityEngine.Rendering.SortingGroup.sortAtRoot : System.Boolean
UnityEngine.Rendering.SortingGroup.sortAtRoot = nil

---@return UnityEngine.Rendering.SortingGroup
function UnityEngine.Rendering.SortingGroup()
end

function UnityEngine.Rendering.SortingGroup.UpdateAllSortingGroups()
end