---@class UnityEngine.Rendering.BatchDrawCommandFlags : System.Enum
UnityEngine.Rendering.BatchDrawCommandFlags = {}

---@field public UnityEngine.Rendering.BatchDrawCommandFlags.value__ : System.Int32
UnityEngine.Rendering.BatchDrawCommandFlags.value__ = nil

---@field public UnityEngine.Rendering.BatchDrawCommandFlags.None : UnityEngine.Rendering.BatchDrawCommandFlags
UnityEngine.Rendering.BatchDrawCommandFlags.None = nil

---@field public UnityEngine.Rendering.BatchDrawCommandFlags.FlipWinding : UnityEngine.Rendering.BatchDrawCommandFlags
UnityEngine.Rendering.BatchDrawCommandFlags.FlipWinding = nil

---@field public UnityEngine.Rendering.BatchDrawCommandFlags.HasMotion : UnityEngine.Rendering.BatchDrawCommandFlags
UnityEngine.Rendering.BatchDrawCommandFlags.HasMotion = nil

---@field public UnityEngine.Rendering.BatchDrawCommandFlags.IsLightMapped : UnityEngine.Rendering.BatchDrawCommandFlags
UnityEngine.Rendering.BatchDrawCommandFlags.IsLightMapped = nil

---@field public UnityEngine.Rendering.BatchDrawCommandFlags.HasSortingPosition : UnityEngine.Rendering.BatchDrawCommandFlags
UnityEngine.Rendering.BatchDrawCommandFlags.HasSortingPosition = nil

---@field public UnityEngine.Rendering.BatchDrawCommandFlags.LODCrossFade : UnityEngine.Rendering.BatchDrawCommandFlags
UnityEngine.Rendering.BatchDrawCommandFlags.LODCrossFade = nil