---@class UnityEngine.Rendering.ShadowSamplingMode : System.Enum
UnityEngine.Rendering.ShadowSamplingMode = {}

---@field public UnityEngine.Rendering.ShadowSamplingMode.value__ : System.Int32
UnityEngine.Rendering.ShadowSamplingMode.value__ = nil

---@field public UnityEngine.Rendering.ShadowSamplingMode.CompareDepths : UnityEngine.Rendering.ShadowSamplingMode
UnityEngine.Rendering.ShadowSamplingMode.CompareDepths = nil

---@field public UnityEngine.Rendering.ShadowSamplingMode.RawDepth : UnityEngine.Rendering.ShadowSamplingMode
UnityEngine.Rendering.ShadowSamplingMode.RawDepth = nil

---@field public UnityEngine.Rendering.ShadowSamplingMode.None : UnityEngine.Rendering.ShadowSamplingMode
UnityEngine.Rendering.ShadowSamplingMode.None = nil