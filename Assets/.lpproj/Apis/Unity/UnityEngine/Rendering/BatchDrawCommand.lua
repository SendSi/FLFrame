---@class UnityEngine.Rendering.BatchDrawCommand : System.ValueType
UnityEngine.Rendering.BatchDrawCommand = {}

---@field public UnityEngine.Rendering.BatchDrawCommand.visibleOffset : System.UInt32
UnityEngine.Rendering.BatchDrawCommand.visibleOffset = nil

---@field public UnityEngine.Rendering.BatchDrawCommand.visibleCount : System.UInt32
UnityEngine.Rendering.BatchDrawCommand.visibleCount = nil

---@field public UnityEngine.Rendering.BatchDrawCommand.batchID : UnityEngine.Rendering.BatchID
UnityEngine.Rendering.BatchDrawCommand.batchID = nil

---@field public UnityEngine.Rendering.BatchDrawCommand.materialID : UnityEngine.Rendering.BatchMaterialID
UnityEngine.Rendering.BatchDrawCommand.materialID = nil

---@field public UnityEngine.Rendering.BatchDrawCommand.meshID : UnityEngine.Rendering.BatchMeshID
UnityEngine.Rendering.BatchDrawCommand.meshID = nil

---@field public UnityEngine.Rendering.BatchDrawCommand.submeshIndex : System.UInt16
UnityEngine.Rendering.BatchDrawCommand.submeshIndex = nil

---@field public UnityEngine.Rendering.BatchDrawCommand.splitVisibilityMask : System.UInt16
UnityEngine.Rendering.BatchDrawCommand.splitVisibilityMask = nil

---@field public UnityEngine.Rendering.BatchDrawCommand.flags : UnityEngine.Rendering.BatchDrawCommandFlags
UnityEngine.Rendering.BatchDrawCommand.flags = nil

---@field public UnityEngine.Rendering.BatchDrawCommand.sortingPosition : System.Int32
UnityEngine.Rendering.BatchDrawCommand.sortingPosition = nil