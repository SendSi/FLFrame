---@class UnityEngine.Rendering.VideoShadersIncludeMode : System.Enum
UnityEngine.Rendering.VideoShadersIncludeMode = {}

---@field public UnityEngine.Rendering.VideoShadersIncludeMode.value__ : System.Int32
UnityEngine.Rendering.VideoShadersIncludeMode.value__ = nil

---@field public UnityEngine.Rendering.VideoShadersIncludeMode.Never : UnityEngine.Rendering.VideoShadersIncludeMode
UnityEngine.Rendering.VideoShadersIncludeMode.Never = nil

---@field public UnityEngine.Rendering.VideoShadersIncludeMode.Referenced : UnityEngine.Rendering.VideoShadersIncludeMode
UnityEngine.Rendering.VideoShadersIncludeMode.Referenced = nil

---@field public UnityEngine.Rendering.VideoShadersIncludeMode.Always : UnityEngine.Rendering.VideoShadersIncludeMode
UnityEngine.Rendering.VideoShadersIncludeMode.Always = nil