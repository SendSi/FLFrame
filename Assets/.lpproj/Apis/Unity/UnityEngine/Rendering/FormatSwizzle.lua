---@class UnityEngine.Rendering.FormatSwizzle : System.Enum
UnityEngine.Rendering.FormatSwizzle = {}

---@field public UnityEngine.Rendering.FormatSwizzle.value__ : System.Int32
UnityEngine.Rendering.FormatSwizzle.value__ = nil

---@field public UnityEngine.Rendering.FormatSwizzle.FormatSwizzleR : UnityEngine.Rendering.FormatSwizzle
UnityEngine.Rendering.FormatSwizzle.FormatSwizzleR = nil

---@field public UnityEngine.Rendering.FormatSwizzle.FormatSwizzleG : UnityEngine.Rendering.FormatSwizzle
UnityEngine.Rendering.FormatSwizzle.FormatSwizzleG = nil

---@field public UnityEngine.Rendering.FormatSwizzle.FormatSwizzleB : UnityEngine.Rendering.FormatSwizzle
UnityEngine.Rendering.FormatSwizzle.FormatSwizzleB = nil

---@field public UnityEngine.Rendering.FormatSwizzle.FormatSwizzleA : UnityEngine.Rendering.FormatSwizzle
UnityEngine.Rendering.FormatSwizzle.FormatSwizzleA = nil

---@field public UnityEngine.Rendering.FormatSwizzle.FormatSwizzle0 : UnityEngine.Rendering.FormatSwizzle
UnityEngine.Rendering.FormatSwizzle.FormatSwizzle0 = nil

---@field public UnityEngine.Rendering.FormatSwizzle.FormatSwizzle1 : UnityEngine.Rendering.FormatSwizzle
UnityEngine.Rendering.FormatSwizzle.FormatSwizzle1 = nil