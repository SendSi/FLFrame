---@class UnityEngine.BlendWeights : System.Enum
UnityEngine.BlendWeights = {}

---@field public UnityEngine.BlendWeights.value__ : System.Int32
UnityEngine.BlendWeights.value__ = nil

---@field public UnityEngine.BlendWeights.OneBone : UnityEngine.BlendWeights
UnityEngine.BlendWeights.OneBone = nil

---@field public UnityEngine.BlendWeights.TwoBones : UnityEngine.BlendWeights
UnityEngine.BlendWeights.TwoBones = nil

---@field public UnityEngine.BlendWeights.FourBones : UnityEngine.BlendWeights
UnityEngine.BlendWeights.FourBones = nil