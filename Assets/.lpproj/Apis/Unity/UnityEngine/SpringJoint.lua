---@class UnityEngine.SpringJoint : UnityEngine.Joint
UnityEngine.SpringJoint = {}

---@property readwrite UnityEngine.SpringJoint.spring : System.Single
UnityEngine.SpringJoint.spring = nil

---@property readwrite UnityEngine.SpringJoint.damper : System.Single
UnityEngine.SpringJoint.damper = nil

---@property readwrite UnityEngine.SpringJoint.minDistance : System.Single
UnityEngine.SpringJoint.minDistance = nil

---@property readwrite UnityEngine.SpringJoint.maxDistance : System.Single
UnityEngine.SpringJoint.maxDistance = nil

---@property readwrite UnityEngine.SpringJoint.tolerance : System.Single
UnityEngine.SpringJoint.tolerance = nil

---@return UnityEngine.SpringJoint
function UnityEngine.SpringJoint()
end