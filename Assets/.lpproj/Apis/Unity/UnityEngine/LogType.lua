---@class UnityEngine.LogType : System.Enum
UnityEngine.LogType = {}

---@field public UnityEngine.LogType.value__ : System.Int32
UnityEngine.LogType.value__ = nil

---@field public UnityEngine.LogType.Error : UnityEngine.LogType
UnityEngine.LogType.Error = nil

---@field public UnityEngine.LogType.Assert : UnityEngine.LogType
UnityEngine.LogType.Assert = nil

---@field public UnityEngine.LogType.Warning : UnityEngine.LogType
UnityEngine.LogType.Warning = nil

---@field public UnityEngine.LogType.Log : UnityEngine.LogType
UnityEngine.LogType.Log = nil

---@field public UnityEngine.LogType.Exception : UnityEngine.LogType
UnityEngine.LogType.Exception = nil