---@class UnityEngine.SleepTimeout : System.Object
UnityEngine.SleepTimeout = {}

---@field public UnityEngine.SleepTimeout.NeverSleep : System.Int32
UnityEngine.SleepTimeout.NeverSleep = nil

---@field public UnityEngine.SleepTimeout.SystemSetting : System.Int32
UnityEngine.SleepTimeout.SystemSetting = nil

---@return UnityEngine.SleepTimeout
function UnityEngine.SleepTimeout()
end