---@class UnityEngine.ColorSpace : System.Enum
UnityEngine.ColorSpace = {}

---@field public UnityEngine.ColorSpace.value__ : System.Int32
UnityEngine.ColorSpace.value__ = nil

---@field public UnityEngine.ColorSpace.Uninitialized : UnityEngine.ColorSpace
UnityEngine.ColorSpace.Uninitialized = nil

---@field public UnityEngine.ColorSpace.Gamma : UnityEngine.ColorSpace
UnityEngine.ColorSpace.Gamma = nil

---@field public UnityEngine.ColorSpace.Linear : UnityEngine.ColorSpace
UnityEngine.ColorSpace.Linear = nil