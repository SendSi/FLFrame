---@class UnityEngine.Windows.WebCam.WebCam : System.Object
UnityEngine.Windows.WebCam.WebCam = {}

---@property readonly UnityEngine.Windows.WebCam.WebCam.Mode : UnityEngine.Windows.WebCam.WebCamMode
UnityEngine.Windows.WebCam.WebCam.Mode = nil

---@return UnityEngine.Windows.WebCam.WebCam
function UnityEngine.Windows.WebCam.WebCam()
end