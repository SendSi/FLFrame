---@class UnityEngine.Windows.WebCam.PhotoCaptureResult : System.ValueType
UnityEngine.Windows.WebCam.PhotoCaptureResult = {}

---@field public UnityEngine.Windows.WebCam.PhotoCaptureResult.resultType : UnityEngine.Windows.WebCam.CaptureResultType
UnityEngine.Windows.WebCam.PhotoCaptureResult.resultType = nil

---@field public UnityEngine.Windows.WebCam.PhotoCaptureResult.hResult : System.Int64
UnityEngine.Windows.WebCam.PhotoCaptureResult.hResult = nil

---@property readonly UnityEngine.Windows.WebCam.PhotoCaptureResult.success : System.Boolean
UnityEngine.Windows.WebCam.PhotoCaptureResult.success = nil