---@class UnityEngine.AudioReverbPreset : System.Enum
UnityEngine.AudioReverbPreset = {}

---@field public UnityEngine.AudioReverbPreset.value__ : System.Int32
UnityEngine.AudioReverbPreset.value__ = nil

---@field public UnityEngine.AudioReverbPreset.Off : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Off = nil

---@field public UnityEngine.AudioReverbPreset.Generic : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Generic = nil

---@field public UnityEngine.AudioReverbPreset.PaddedCell : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.PaddedCell = nil

---@field public UnityEngine.AudioReverbPreset.Room : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Room = nil

---@field public UnityEngine.AudioReverbPreset.Bathroom : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Bathroom = nil

---@field public UnityEngine.AudioReverbPreset.Livingroom : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Livingroom = nil

---@field public UnityEngine.AudioReverbPreset.Stoneroom : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Stoneroom = nil

---@field public UnityEngine.AudioReverbPreset.Auditorium : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Auditorium = nil

---@field public UnityEngine.AudioReverbPreset.Concerthall : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Concerthall = nil

---@field public UnityEngine.AudioReverbPreset.Cave : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Cave = nil

---@field public UnityEngine.AudioReverbPreset.Arena : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Arena = nil

---@field public UnityEngine.AudioReverbPreset.Hangar : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Hangar = nil

---@field public UnityEngine.AudioReverbPreset.CarpetedHallway : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.CarpetedHallway = nil

---@field public UnityEngine.AudioReverbPreset.Hallway : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Hallway = nil

---@field public UnityEngine.AudioReverbPreset.StoneCorridor : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.StoneCorridor = nil

---@field public UnityEngine.AudioReverbPreset.Alley : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Alley = nil

---@field public UnityEngine.AudioReverbPreset.Forest : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Forest = nil

---@field public UnityEngine.AudioReverbPreset.City : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.City = nil

---@field public UnityEngine.AudioReverbPreset.Mountains : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Mountains = nil

---@field public UnityEngine.AudioReverbPreset.Quarry : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Quarry = nil

---@field public UnityEngine.AudioReverbPreset.Plain : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Plain = nil

---@field public UnityEngine.AudioReverbPreset.ParkingLot : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.ParkingLot = nil

---@field public UnityEngine.AudioReverbPreset.SewerPipe : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.SewerPipe = nil

---@field public UnityEngine.AudioReverbPreset.Underwater : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Underwater = nil

---@field public UnityEngine.AudioReverbPreset.Drugged : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Drugged = nil

---@field public UnityEngine.AudioReverbPreset.Dizzy : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Dizzy = nil

---@field public UnityEngine.AudioReverbPreset.Psychotic : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.Psychotic = nil

---@field public UnityEngine.AudioReverbPreset.User : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbPreset.User = nil