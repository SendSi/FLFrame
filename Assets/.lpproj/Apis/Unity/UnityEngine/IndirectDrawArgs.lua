---@class UnityEngine.IndirectDrawArgs : System.ValueType
UnityEngine.IndirectDrawArgs = {}

---@field public UnityEngine.IndirectDrawArgs.size : System.Int32
UnityEngine.IndirectDrawArgs.size = nil

---@property readwrite UnityEngine.IndirectDrawArgs.vertexCountPerInstance : System.UInt32
UnityEngine.IndirectDrawArgs.vertexCountPerInstance = nil

---@property readwrite UnityEngine.IndirectDrawArgs.instanceCount : System.UInt32
UnityEngine.IndirectDrawArgs.instanceCount = nil

---@property readwrite UnityEngine.IndirectDrawArgs.startVertex : System.UInt32
UnityEngine.IndirectDrawArgs.startVertex = nil

---@property readwrite UnityEngine.IndirectDrawArgs.startInstance : System.UInt32
UnityEngine.IndirectDrawArgs.startInstance = nil