---@class UnityEngine.ExecuteInEditMode : System.Attribute
UnityEngine.ExecuteInEditMode = {}

---@return UnityEngine.ExecuteInEditMode
function UnityEngine.ExecuteInEditMode()
end