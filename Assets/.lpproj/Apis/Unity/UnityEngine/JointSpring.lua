---@class UnityEngine.JointSpring : System.ValueType
UnityEngine.JointSpring = {}

---@field public UnityEngine.JointSpring.spring : System.Single
UnityEngine.JointSpring.spring = nil

---@field public UnityEngine.JointSpring.damper : System.Single
UnityEngine.JointSpring.damper = nil

---@field public UnityEngine.JointSpring.targetPosition : System.Single
UnityEngine.JointSpring.targetPosition = nil