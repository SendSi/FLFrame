---@class UnityEngine.OcclusionPortal : UnityEngine.Component
UnityEngine.OcclusionPortal = {}

---@property readwrite UnityEngine.OcclusionPortal.open : System.Boolean
UnityEngine.OcclusionPortal.open = nil

---@return UnityEngine.OcclusionPortal
function UnityEngine.OcclusionPortal()
end