---@class UnityEngine.Lumin.UsesLuminPrivilegeAttribute : System.Attribute
UnityEngine.Lumin.UsesLuminPrivilegeAttribute = {}

---@property readonly UnityEngine.Lumin.UsesLuminPrivilegeAttribute.privilege : System.String
UnityEngine.Lumin.UsesLuminPrivilegeAttribute.privilege = nil

---@param privilege : System.String
---@return UnityEngine.Lumin.UsesLuminPrivilegeAttribute
function UnityEngine.Lumin.UsesLuminPrivilegeAttribute(privilege)
end