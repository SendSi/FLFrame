---@module UnityEngine.Lumin
UnityEngine.Lumin = {}

---@class UnityEngine.Lumin.UsesLuminPlatformLevelAttribute : System.Attribute
UnityEngine.Lumin.UsesLuminPlatformLevelAttribute = {}

---@property readonly UnityEngine.Lumin.UsesLuminPlatformLevelAttribute.platformLevel : System.UInt32
UnityEngine.Lumin.UsesLuminPlatformLevelAttribute.platformLevel = nil

---@param platformLevel : System.UInt32
---@return UnityEngine.Lumin.UsesLuminPlatformLevelAttribute
function UnityEngine.Lumin.UsesLuminPlatformLevelAttribute(platformLevel)
end