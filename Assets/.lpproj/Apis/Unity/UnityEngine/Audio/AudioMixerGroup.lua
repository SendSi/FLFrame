---@class UnityEngine.Audio.AudioMixerGroup : UnityEngine.Object
UnityEngine.Audio.AudioMixerGroup = {}

---@property readonly UnityEngine.Audio.AudioMixerGroup.audioMixer : UnityEngine.Audio.AudioMixer
UnityEngine.Audio.AudioMixerGroup.audioMixer = nil