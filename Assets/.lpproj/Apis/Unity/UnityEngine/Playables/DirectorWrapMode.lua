---@class UnityEngine.Playables.DirectorWrapMode : System.Enum
UnityEngine.Playables.DirectorWrapMode = {}

---@field public UnityEngine.Playables.DirectorWrapMode.value__ : System.Int32
UnityEngine.Playables.DirectorWrapMode.value__ = nil

---@field public UnityEngine.Playables.DirectorWrapMode.Hold : UnityEngine.Playables.DirectorWrapMode
UnityEngine.Playables.DirectorWrapMode.Hold = nil

---@field public UnityEngine.Playables.DirectorWrapMode.Loop : UnityEngine.Playables.DirectorWrapMode
UnityEngine.Playables.DirectorWrapMode.Loop = nil

---@field public UnityEngine.Playables.DirectorWrapMode.None : UnityEngine.Playables.DirectorWrapMode
UnityEngine.Playables.DirectorWrapMode.None = nil