---@class UnityEngine.Playables.PlayableHandle : System.ValueType
UnityEngine.Playables.PlayableHandle = {}

---@property readonly UnityEngine.Playables.PlayableHandle.Null : UnityEngine.Playables.PlayableHandle
UnityEngine.Playables.PlayableHandle.Null = nil

---@param x : UnityEngine.Playables.PlayableHandle
---@param y : UnityEngine.Playables.PlayableHandle
---@return System.Boolean
function UnityEngine.Playables.PlayableHandle.op_Equality(x, y)
end

---@param x : UnityEngine.Playables.PlayableHandle
---@param y : UnityEngine.Playables.PlayableHandle
---@return System.Boolean
function UnityEngine.Playables.PlayableHandle.op_Inequality(x, y)
end

---@param p : System.Object
---@return System.Boolean
function UnityEngine.Playables.PlayableHandle:Equals(p)
end

---@param other : UnityEngine.Playables.PlayableHandle
---@return System.Boolean
function UnityEngine.Playables.PlayableHandle:Equals(other)
end

---@return System.Int32
function UnityEngine.Playables.PlayableHandle:GetHashCode()
end