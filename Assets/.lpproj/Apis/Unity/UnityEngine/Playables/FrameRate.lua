---@class UnityEngine.Playables.FrameRate : System.ValueType
UnityEngine.Playables.FrameRate = {}

---@field public UnityEngine.Playables.FrameRate.k_24Fps : UnityEngine.Playables.FrameRate
UnityEngine.Playables.FrameRate.k_24Fps = nil

---@field public UnityEngine.Playables.FrameRate.k_23_976Fps : UnityEngine.Playables.FrameRate
UnityEngine.Playables.FrameRate.k_23_976Fps = nil

---@field public UnityEngine.Playables.FrameRate.k_25Fps : UnityEngine.Playables.FrameRate
UnityEngine.Playables.FrameRate.k_25Fps = nil

---@field public UnityEngine.Playables.FrameRate.k_30Fps : UnityEngine.Playables.FrameRate
UnityEngine.Playables.FrameRate.k_30Fps = nil

---@field public UnityEngine.Playables.FrameRate.k_29_97Fps : UnityEngine.Playables.FrameRate
UnityEngine.Playables.FrameRate.k_29_97Fps = nil

---@field public UnityEngine.Playables.FrameRate.k_50Fps : UnityEngine.Playables.FrameRate
UnityEngine.Playables.FrameRate.k_50Fps = nil

---@field public UnityEngine.Playables.FrameRate.k_60Fps : UnityEngine.Playables.FrameRate
UnityEngine.Playables.FrameRate.k_60Fps = nil

---@field public UnityEngine.Playables.FrameRate.k_59_94Fps : UnityEngine.Playables.FrameRate
UnityEngine.Playables.FrameRate.k_59_94Fps = nil

---@property readonly UnityEngine.Playables.FrameRate.dropFrame : System.Boolean
UnityEngine.Playables.FrameRate.dropFrame = nil

---@property readonly UnityEngine.Playables.FrameRate.rate : System.Double
UnityEngine.Playables.FrameRate.rate = nil

---@param frameRate : System.UInt32
---@param drop : System.Boolean
---@return UnityEngine.Playables.FrameRate
function UnityEngine.Playables.FrameRate(frameRate, drop)
end

---@return System.Boolean
function UnityEngine.Playables.FrameRate:IsValid()
end

---@param other : UnityEngine.Playables.FrameRate
---@return System.Boolean
function UnityEngine.Playables.FrameRate:Equals(other)
end

---@param obj : System.Object
---@return System.Boolean
function UnityEngine.Playables.FrameRate:Equals(obj)
end

---@param a : UnityEngine.Playables.FrameRate
---@param b : UnityEngine.Playables.FrameRate
---@return System.Boolean
function UnityEngine.Playables.FrameRate.op_Equality(a, b)
end

---@param a : UnityEngine.Playables.FrameRate
---@param b : UnityEngine.Playables.FrameRate
---@return System.Boolean
function UnityEngine.Playables.FrameRate.op_Inequality(a, b)
end

---@param a : UnityEngine.Playables.FrameRate
---@param b : UnityEngine.Playables.FrameRate
---@return System.Boolean
function UnityEngine.Playables.FrameRate.op_LessThan(a, b)
end

---@param a : UnityEngine.Playables.FrameRate
---@param b : UnityEngine.Playables.FrameRate
---@return System.Boolean
function UnityEngine.Playables.FrameRate.op_LessThanOrEqual(a, b)
end

---@param a : UnityEngine.Playables.FrameRate
---@param b : UnityEngine.Playables.FrameRate
---@return System.Boolean
function UnityEngine.Playables.FrameRate.op_GreaterThan(a, b)
end

---@param a : UnityEngine.Playables.FrameRate
---@param b : UnityEngine.Playables.FrameRate
---@return System.Boolean
function UnityEngine.Playables.FrameRate.op_GreaterThanOrEqual(a, b)
end

---@return System.Int32
function UnityEngine.Playables.FrameRate:GetHashCode()
end

---@return System.String
function UnityEngine.Playables.FrameRate:ToString()
end

---@param format : System.String
---@return System.String
function UnityEngine.Playables.FrameRate:ToString(format)
end

---@param format : System.String
---@param formatProvider : System.IFormatProvider
---@return System.String
function UnityEngine.Playables.FrameRate:ToString(format, formatProvider)
end