---@class UnityEngine.iPhoneAccelerationEvent : System.ValueType
UnityEngine.iPhoneAccelerationEvent = {}

---@property readonly UnityEngine.iPhoneAccelerationEvent.timeDelta : System.Single
UnityEngine.iPhoneAccelerationEvent.timeDelta = nil

---@property readonly UnityEngine.iPhoneAccelerationEvent.acceleration : UnityEngine.Vector3
UnityEngine.iPhoneAccelerationEvent.acceleration = nil

---@property readonly UnityEngine.iPhoneAccelerationEvent.deltaTime : System.Single
UnityEngine.iPhoneAccelerationEvent.deltaTime = nil