---@class UnityEngine.BoneWeight : System.ValueType
UnityEngine.BoneWeight = {}

---@property readwrite UnityEngine.BoneWeight.weight0 : System.Single
UnityEngine.BoneWeight.weight0 = nil

---@property readwrite UnityEngine.BoneWeight.weight1 : System.Single
UnityEngine.BoneWeight.weight1 = nil

---@property readwrite UnityEngine.BoneWeight.weight2 : System.Single
UnityEngine.BoneWeight.weight2 = nil

---@property readwrite UnityEngine.BoneWeight.weight3 : System.Single
UnityEngine.BoneWeight.weight3 = nil

---@property readwrite UnityEngine.BoneWeight.boneIndex0 : System.Int32
UnityEngine.BoneWeight.boneIndex0 = nil

---@property readwrite UnityEngine.BoneWeight.boneIndex1 : System.Int32
UnityEngine.BoneWeight.boneIndex1 = nil

---@property readwrite UnityEngine.BoneWeight.boneIndex2 : System.Int32
UnityEngine.BoneWeight.boneIndex2 = nil

---@property readwrite UnityEngine.BoneWeight.boneIndex3 : System.Int32
UnityEngine.BoneWeight.boneIndex3 = nil

---@return System.Int32
function UnityEngine.BoneWeight:GetHashCode()
end

---@param other : System.Object
---@return System.Boolean
function UnityEngine.BoneWeight:Equals(other)
end

---@param other : UnityEngine.BoneWeight
---@return System.Boolean
function UnityEngine.BoneWeight:Equals(other)
end

---@param lhs : UnityEngine.BoneWeight
---@param rhs : UnityEngine.BoneWeight
---@return System.Boolean
function UnityEngine.BoneWeight.op_Equality(lhs, rhs)
end

---@param lhs : UnityEngine.BoneWeight
---@param rhs : UnityEngine.BoneWeight
---@return System.Boolean
function UnityEngine.BoneWeight.op_Inequality(lhs, rhs)
end