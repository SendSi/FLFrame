---@module UnityEngine.IO
UnityEngine.IO = {}

---@class UnityEngine.IO.ThreadIORestrictionMode : System.Enum
UnityEngine.IO.ThreadIORestrictionMode = {}

---@field public UnityEngine.IO.ThreadIORestrictionMode.value__ : System.Int32
UnityEngine.IO.ThreadIORestrictionMode.value__ = nil

---@field public UnityEngine.IO.ThreadIORestrictionMode.Allowed : UnityEngine.IO.ThreadIORestrictionMode
UnityEngine.IO.ThreadIORestrictionMode.Allowed = nil

---@field public UnityEngine.IO.ThreadIORestrictionMode.TreatAsError : UnityEngine.IO.ThreadIORestrictionMode
UnityEngine.IO.ThreadIORestrictionMode.TreatAsError = nil