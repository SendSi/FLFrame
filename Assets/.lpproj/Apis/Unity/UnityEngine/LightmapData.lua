---@class UnityEngine.LightmapData : System.Object
UnityEngine.LightmapData = {}

---@property readwrite UnityEngine.LightmapData.lightmapLight : UnityEngine.Texture2D
UnityEngine.LightmapData.lightmapLight = nil

---@property readwrite UnityEngine.LightmapData.lightmapColor : UnityEngine.Texture2D
UnityEngine.LightmapData.lightmapColor = nil

---@property readwrite UnityEngine.LightmapData.lightmapDir : UnityEngine.Texture2D
UnityEngine.LightmapData.lightmapDir = nil

---@property readwrite UnityEngine.LightmapData.shadowMask : UnityEngine.Texture2D
UnityEngine.LightmapData.shadowMask = nil

---@property readwrite UnityEngine.LightmapData.lightmap : UnityEngine.Texture2D
UnityEngine.LightmapData.lightmap = nil

---@property readwrite UnityEngine.LightmapData.lightmapFar : UnityEngine.Texture2D
UnityEngine.LightmapData.lightmapFar = nil

---@property readwrite UnityEngine.LightmapData.lightmapNear : UnityEngine.Texture2D
UnityEngine.LightmapData.lightmapNear = nil

---@return UnityEngine.LightmapData
function UnityEngine.LightmapData()
end