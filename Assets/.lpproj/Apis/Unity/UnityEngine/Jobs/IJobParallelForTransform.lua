---@module UnityEngine.Jobs
UnityEngine.Jobs = {}

---@class UnityEngine.Jobs.IJobParallelForTransform
UnityEngine.Jobs.IJobParallelForTransform = {}

---@param index : System.Int32
---@param transform : UnityEngine.Jobs.TransformAccess
function UnityEngine.Jobs.IJobParallelForTransform:Execute(index, transform)
end