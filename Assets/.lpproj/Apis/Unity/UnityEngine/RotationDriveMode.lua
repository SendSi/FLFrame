---@class UnityEngine.RotationDriveMode : System.Enum
UnityEngine.RotationDriveMode = {}

---@field public UnityEngine.RotationDriveMode.value__ : System.Int32
UnityEngine.RotationDriveMode.value__ = nil

---@field public UnityEngine.RotationDriveMode.XYAndZ : UnityEngine.RotationDriveMode
UnityEngine.RotationDriveMode.XYAndZ = nil

---@field public UnityEngine.RotationDriveMode.Slerp : UnityEngine.RotationDriveMode
UnityEngine.RotationDriveMode.Slerp = nil