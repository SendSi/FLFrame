---@class UnityEngine.WSA.WindowActivationState : System.Enum
UnityEngine.WSA.WindowActivationState = {}

---@field public UnityEngine.WSA.WindowActivationState.value__ : System.Int32
UnityEngine.WSA.WindowActivationState.value__ = nil

---@field public UnityEngine.WSA.WindowActivationState.CodeActivated : UnityEngine.WSA.WindowActivationState
UnityEngine.WSA.WindowActivationState.CodeActivated = nil

---@field public UnityEngine.WSA.WindowActivationState.Deactivated : UnityEngine.WSA.WindowActivationState
UnityEngine.WSA.WindowActivationState.Deactivated = nil

---@field public UnityEngine.WSA.WindowActivationState.PointerActivated : UnityEngine.WSA.WindowActivationState
UnityEngine.WSA.WindowActivationState.PointerActivated = nil