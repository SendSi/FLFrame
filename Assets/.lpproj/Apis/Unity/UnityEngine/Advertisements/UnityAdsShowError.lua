---@class UnityEngine.Advertisements.UnityAdsShowError : System.Enum
UnityEngine.Advertisements.UnityAdsShowError = {}

---@field public UnityEngine.Advertisements.UnityAdsShowError.value__ : System.Int32
UnityEngine.Advertisements.UnityAdsShowError.value__ = nil

---@field public UnityEngine.Advertisements.UnityAdsShowError.NOT_INITIALIZED : UnityEngine.Advertisements.UnityAdsShowError
UnityEngine.Advertisements.UnityAdsShowError.NOT_INITIALIZED = nil

---@field public UnityEngine.Advertisements.UnityAdsShowError.NOT_READY : UnityEngine.Advertisements.UnityAdsShowError
UnityEngine.Advertisements.UnityAdsShowError.NOT_READY = nil

---@field public UnityEngine.Advertisements.UnityAdsShowError.VIDEO_PLAYER_ERROR : UnityEngine.Advertisements.UnityAdsShowError
UnityEngine.Advertisements.UnityAdsShowError.VIDEO_PLAYER_ERROR = nil

---@field public UnityEngine.Advertisements.UnityAdsShowError.INVALID_ARGUMENT : UnityEngine.Advertisements.UnityAdsShowError
UnityEngine.Advertisements.UnityAdsShowError.INVALID_ARGUMENT = nil

---@field public UnityEngine.Advertisements.UnityAdsShowError.NO_CONNECTION : UnityEngine.Advertisements.UnityAdsShowError
UnityEngine.Advertisements.UnityAdsShowError.NO_CONNECTION = nil

---@field public UnityEngine.Advertisements.UnityAdsShowError.ALREADY_SHOWING : UnityEngine.Advertisements.UnityAdsShowError
UnityEngine.Advertisements.UnityAdsShowError.ALREADY_SHOWING = nil

---@field public UnityEngine.Advertisements.UnityAdsShowError.INTERNAL_ERROR : UnityEngine.Advertisements.UnityAdsShowError
UnityEngine.Advertisements.UnityAdsShowError.INTERNAL_ERROR = nil

---@field public UnityEngine.Advertisements.UnityAdsShowError.UNKNOWN : UnityEngine.Advertisements.UnityAdsShowError
UnityEngine.Advertisements.UnityAdsShowError.UNKNOWN = nil