---@class UnityEngine.Advertisements.ShowResult : System.Enum
UnityEngine.Advertisements.ShowResult = {}

---@field public UnityEngine.Advertisements.ShowResult.value__ : System.Int32
UnityEngine.Advertisements.ShowResult.value__ = nil

---@field public UnityEngine.Advertisements.ShowResult.Failed : UnityEngine.Advertisements.ShowResult
UnityEngine.Advertisements.ShowResult.Failed = nil

---@field public UnityEngine.Advertisements.ShowResult.Skipped : UnityEngine.Advertisements.ShowResult
UnityEngine.Advertisements.ShowResult.Skipped = nil

---@field public UnityEngine.Advertisements.ShowResult.Finished : UnityEngine.Advertisements.ShowResult
UnityEngine.Advertisements.ShowResult.Finished = nil