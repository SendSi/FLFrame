---@class UnityEngine.Advertisements.UnityAdsCompletionState : System.Enum
UnityEngine.Advertisements.UnityAdsCompletionState = {}

---@field public UnityEngine.Advertisements.UnityAdsCompletionState.value__ : System.Int32
UnityEngine.Advertisements.UnityAdsCompletionState.value__ = nil

---@field public UnityEngine.Advertisements.UnityAdsCompletionState.SKIPPED : UnityEngine.Advertisements.UnityAdsCompletionState
UnityEngine.Advertisements.UnityAdsCompletionState.SKIPPED = nil

---@field public UnityEngine.Advertisements.UnityAdsCompletionState.COMPLETED : UnityEngine.Advertisements.UnityAdsCompletionState
UnityEngine.Advertisements.UnityAdsCompletionState.COMPLETED = nil

---@field public UnityEngine.Advertisements.UnityAdsCompletionState.UNKNOWN : UnityEngine.Advertisements.UnityAdsCompletionState
UnityEngine.Advertisements.UnityAdsCompletionState.UNKNOWN = nil