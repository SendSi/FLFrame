---@class UnityEngine.StaticBatchingUtility : System.Object
UnityEngine.StaticBatchingUtility = {}

---@return UnityEngine.StaticBatchingUtility
function UnityEngine.StaticBatchingUtility()
end

---@param staticBatchRoot : UnityEngine.GameObject
function UnityEngine.StaticBatchingUtility.Combine(staticBatchRoot)
end

---@param gos : UnityEngine.GameObject[]
---@param staticBatchRoot : UnityEngine.GameObject
function UnityEngine.StaticBatchingUtility.Combine(gos, staticBatchRoot)
end