---@module UnityEngine.Search
UnityEngine.Search = {}

---@class UnityEngine.Search.SearchViewFlags : System.Enum
UnityEngine.Search.SearchViewFlags = {}

---@field public UnityEngine.Search.SearchViewFlags.value__ : System.Int32
UnityEngine.Search.SearchViewFlags.value__ = nil

---@field public UnityEngine.Search.SearchViewFlags.None : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.None = nil

---@field public UnityEngine.Search.SearchViewFlags.Debug : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.Debug = nil

---@field public UnityEngine.Search.SearchViewFlags.NoIndexing : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.NoIndexing = nil

---@field public UnityEngine.Search.SearchViewFlags.Packages : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.Packages = nil

---@field public UnityEngine.Search.SearchViewFlags.OpenLeftSidePanel : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.OpenLeftSidePanel = nil

---@field public UnityEngine.Search.SearchViewFlags.OpenInspectorPreview : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.OpenInspectorPreview = nil

---@field public UnityEngine.Search.SearchViewFlags.Centered : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.Centered = nil

---@field public UnityEngine.Search.SearchViewFlags.HideSearchBar : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.HideSearchBar = nil

---@field public UnityEngine.Search.SearchViewFlags.CompactView : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.CompactView = nil

---@field public UnityEngine.Search.SearchViewFlags.ListView : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.ListView = nil

---@field public UnityEngine.Search.SearchViewFlags.GridView : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.GridView = nil

---@field public UnityEngine.Search.SearchViewFlags.TableView : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.TableView = nil

---@field public UnityEngine.Search.SearchViewFlags.EnableSearchQuery : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.EnableSearchQuery = nil

---@field public UnityEngine.Search.SearchViewFlags.DisableInspectorPreview : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.DisableInspectorPreview = nil

---@field public UnityEngine.Search.SearchViewFlags.DisableSavedSearchQuery : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.DisableSavedSearchQuery = nil

---@field public UnityEngine.Search.SearchViewFlags.OpenInBuilderMode : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.OpenInBuilderMode = nil

---@field public UnityEngine.Search.SearchViewFlags.OpenInTextMode : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.OpenInTextMode = nil

---@field public UnityEngine.Search.SearchViewFlags.DisableBuilderModeToggle : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.DisableBuilderModeToggle = nil

---@field public UnityEngine.Search.SearchViewFlags.Borderless : UnityEngine.Search.SearchViewFlags
UnityEngine.Search.SearchViewFlags.Borderless = nil