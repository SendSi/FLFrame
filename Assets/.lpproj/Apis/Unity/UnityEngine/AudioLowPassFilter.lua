---@class UnityEngine.AudioLowPassFilter : UnityEngine.Behaviour
UnityEngine.AudioLowPassFilter = {}

---@property readwrite UnityEngine.AudioLowPassFilter.lowpassResonaceQ : System.Single
UnityEngine.AudioLowPassFilter.lowpassResonaceQ = nil

---@property readwrite UnityEngine.AudioLowPassFilter.customCutoffCurve : UnityEngine.AnimationCurve
UnityEngine.AudioLowPassFilter.customCutoffCurve = nil

---@property readwrite UnityEngine.AudioLowPassFilter.cutoffFrequency : System.Single
UnityEngine.AudioLowPassFilter.cutoffFrequency = nil

---@property readwrite UnityEngine.AudioLowPassFilter.lowpassResonanceQ : System.Single
UnityEngine.AudioLowPassFilter.lowpassResonanceQ = nil

---@return UnityEngine.AudioLowPassFilter
function UnityEngine.AudioLowPassFilter()
end