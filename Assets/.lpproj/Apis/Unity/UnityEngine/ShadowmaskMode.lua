---@class UnityEngine.ShadowmaskMode : System.Enum
UnityEngine.ShadowmaskMode = {}

---@field public UnityEngine.ShadowmaskMode.value__ : System.Int32
UnityEngine.ShadowmaskMode.value__ = nil

---@field public UnityEngine.ShadowmaskMode.Shadowmask : UnityEngine.ShadowmaskMode
UnityEngine.ShadowmaskMode.Shadowmask = nil

---@field public UnityEngine.ShadowmaskMode.DistanceShadowmask : UnityEngine.ShadowmaskMode
UnityEngine.ShadowmaskMode.DistanceShadowmask = nil