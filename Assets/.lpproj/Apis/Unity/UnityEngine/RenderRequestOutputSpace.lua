---@class UnityEngine.RenderRequestOutputSpace : System.Enum
UnityEngine.RenderRequestOutputSpace = {}

---@field public UnityEngine.RenderRequestOutputSpace.value__ : System.Int32
UnityEngine.RenderRequestOutputSpace.value__ = nil

---@field public UnityEngine.RenderRequestOutputSpace.ScreenSpace : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.ScreenSpace = nil

---@field public UnityEngine.RenderRequestOutputSpace.UV0 : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.UV0 = nil

---@field public UnityEngine.RenderRequestOutputSpace.UV1 : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.UV1 = nil

---@field public UnityEngine.RenderRequestOutputSpace.UV2 : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.UV2 = nil

---@field public UnityEngine.RenderRequestOutputSpace.UV3 : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.UV3 = nil

---@field public UnityEngine.RenderRequestOutputSpace.UV4 : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.UV4 = nil

---@field public UnityEngine.RenderRequestOutputSpace.UV5 : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.UV5 = nil

---@field public UnityEngine.RenderRequestOutputSpace.UV6 : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.UV6 = nil

---@field public UnityEngine.RenderRequestOutputSpace.UV7 : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.UV7 = nil

---@field public UnityEngine.RenderRequestOutputSpace.UV8 : UnityEngine.RenderRequestOutputSpace
UnityEngine.RenderRequestOutputSpace.UV8 = nil