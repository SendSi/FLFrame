---@class UnityEngine.iOS.InterstitialWasViewedDelegate : System.MulticastDelegate
UnityEngine.iOS.InterstitialWasViewedDelegate = {}

---@param object : System.Object
---@param method : System.IntPtr
---@return UnityEngine.iOS.InterstitialWasViewedDelegate
function UnityEngine.iOS.InterstitialWasViewedDelegate(object, method)
end

function UnityEngine.iOS.InterstitialWasViewedDelegate:Invoke()
end

---@param callback : System.AsyncCallback
---@param object : System.Object
---@return System.IAsyncResult
function UnityEngine.iOS.InterstitialWasViewedDelegate:BeginInvoke(callback, object)
end

---@param result : System.IAsyncResult
function UnityEngine.iOS.InterstitialWasViewedDelegate:EndInvoke(result)
end