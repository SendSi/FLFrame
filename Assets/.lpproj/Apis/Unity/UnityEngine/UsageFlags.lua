---@class UnityEngine.UsageFlags : System.Enum
UnityEngine.UsageFlags = {}

---@field public UnityEngine.UsageFlags.value__ : System.Int32
UnityEngine.UsageFlags.value__ = nil

---@field public UnityEngine.UsageFlags.None : UnityEngine.UsageFlags
UnityEngine.UsageFlags.None = nil

---@field public UnityEngine.UsageFlags.LockBufferForWrite : UnityEngine.UsageFlags
UnityEngine.UsageFlags.LockBufferForWrite = nil