---@class UnityEngine.Sampling : System.Enum
UnityEngine.Sampling = {}

---@field public UnityEngine.Sampling.value__ : System.Int32
UnityEngine.Sampling.value__ = nil

---@field public UnityEngine.Sampling.Auto : UnityEngine.Sampling
UnityEngine.Sampling.Auto = nil

---@field public UnityEngine.Sampling.Fixed : UnityEngine.Sampling
UnityEngine.Sampling.Fixed = nil