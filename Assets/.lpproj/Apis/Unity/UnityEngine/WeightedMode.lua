---@class UnityEngine.WeightedMode : System.Enum
UnityEngine.WeightedMode = {}

---@field public UnityEngine.WeightedMode.value__ : System.Int32
UnityEngine.WeightedMode.value__ = nil

---@field public UnityEngine.WeightedMode.None : UnityEngine.WeightedMode
UnityEngine.WeightedMode.None = nil

---@field public UnityEngine.WeightedMode.In : UnityEngine.WeightedMode
UnityEngine.WeightedMode.In = nil

---@field public UnityEngine.WeightedMode.Out : UnityEngine.WeightedMode
UnityEngine.WeightedMode.Out = nil

---@field public UnityEngine.WeightedMode.Both : UnityEngine.WeightedMode
UnityEngine.WeightedMode.Both = nil