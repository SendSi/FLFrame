---@class UnityEngine.GateFitMode : System.Enum
UnityEngine.GateFitMode = {}

---@field public UnityEngine.GateFitMode.value__ : System.Int32
UnityEngine.GateFitMode.value__ = nil

---@field public UnityEngine.GateFitMode.Vertical : UnityEngine.GateFitMode
UnityEngine.GateFitMode.Vertical = nil

---@field public UnityEngine.GateFitMode.Horizontal : UnityEngine.GateFitMode
UnityEngine.GateFitMode.Horizontal = nil

---@field public UnityEngine.GateFitMode.Fill : UnityEngine.GateFitMode
UnityEngine.GateFitMode.Fill = nil

---@field public UnityEngine.GateFitMode.Overscan : UnityEngine.GateFitMode
UnityEngine.GateFitMode.Overscan = nil

---@field public UnityEngine.GateFitMode.None : UnityEngine.GateFitMode
UnityEngine.GateFitMode.None = nil