---@class UnityEngine.IconAttribute : System.Attribute
UnityEngine.IconAttribute = {}

---@property readonly UnityEngine.IconAttribute.path : System.String
UnityEngine.IconAttribute.path = nil

---@param path : System.String
---@return UnityEngine.IconAttribute
function UnityEngine.IconAttribute(path)
end