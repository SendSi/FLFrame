---@class UnityEngine.ResourceRequest : UnityEngine.AsyncOperation
UnityEngine.ResourceRequest = {}

---@property readonly UnityEngine.ResourceRequest.asset : UnityEngine.Object
UnityEngine.ResourceRequest.asset = nil

---@return UnityEngine.ResourceRequest
function UnityEngine.ResourceRequest()
end