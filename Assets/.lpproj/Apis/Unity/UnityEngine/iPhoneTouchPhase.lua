---@class UnityEngine.iPhoneTouchPhase : System.Enum
UnityEngine.iPhoneTouchPhase = {}

---@field public UnityEngine.iPhoneTouchPhase.value__ : System.Int32
UnityEngine.iPhoneTouchPhase.value__ = nil

---@field public UnityEngine.iPhoneTouchPhase.Began : UnityEngine.iPhoneTouchPhase
UnityEngine.iPhoneTouchPhase.Began = nil

---@field public UnityEngine.iPhoneTouchPhase.Moved : UnityEngine.iPhoneTouchPhase
UnityEngine.iPhoneTouchPhase.Moved = nil

---@field public UnityEngine.iPhoneTouchPhase.Stationary : UnityEngine.iPhoneTouchPhase
UnityEngine.iPhoneTouchPhase.Stationary = nil

---@field public UnityEngine.iPhoneTouchPhase.Ended : UnityEngine.iPhoneTouchPhase
UnityEngine.iPhoneTouchPhase.Ended = nil

---@field public UnityEngine.iPhoneTouchPhase.Canceled : UnityEngine.iPhoneTouchPhase
UnityEngine.iPhoneTouchPhase.Canceled = nil