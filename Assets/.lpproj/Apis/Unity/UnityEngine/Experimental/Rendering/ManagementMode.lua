---@class UnityEngine.Experimental.Rendering.ManagementMode : System.Enum
UnityEngine.Experimental.Rendering.ManagementMode = {}

---@field public UnityEngine.Experimental.Rendering.ManagementMode.value__ : System.Int32
UnityEngine.Experimental.Rendering.ManagementMode.value__ = nil

---@field public UnityEngine.Experimental.Rendering.ManagementMode.Manual : UnityEngine.Experimental.Rendering.ManagementMode
UnityEngine.Experimental.Rendering.ManagementMode.Manual = nil

---@field public UnityEngine.Experimental.Rendering.ManagementMode.Automatic : UnityEngine.Experimental.Rendering.ManagementMode
UnityEngine.Experimental.Rendering.ManagementMode.Automatic = nil