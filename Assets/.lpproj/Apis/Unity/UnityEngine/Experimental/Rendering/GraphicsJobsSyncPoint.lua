---@class UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint : System.Enum
UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint = {}

---@field public UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.value__ : System.Int32
UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.value__ = nil

---@field public UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.EndOfFrame : UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint
UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.EndOfFrame = nil

---@field public UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.AfterScriptUpdate : UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint
UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.AfterScriptUpdate = nil

---@field public UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.AfterScriptLateUpdate : UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint
UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.AfterScriptLateUpdate = nil

---@field public UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.WaitForPresent : UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint
UnityEngine.Experimental.Rendering.GraphicsJobsSyncPoint.WaitForPresent = nil