---@class UnityEngine.Experimental.Rendering.RayTracingModeMask : System.Enum
UnityEngine.Experimental.Rendering.RayTracingModeMask = {}

---@field public UnityEngine.Experimental.Rendering.RayTracingModeMask.value__ : System.Int32
UnityEngine.Experimental.Rendering.RayTracingModeMask.value__ = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingModeMask.Nothing : UnityEngine.Experimental.Rendering.RayTracingModeMask
UnityEngine.Experimental.Rendering.RayTracingModeMask.Nothing = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingModeMask.Static : UnityEngine.Experimental.Rendering.RayTracingModeMask
UnityEngine.Experimental.Rendering.RayTracingModeMask.Static = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingModeMask.DynamicTransform : UnityEngine.Experimental.Rendering.RayTracingModeMask
UnityEngine.Experimental.Rendering.RayTracingModeMask.DynamicTransform = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingModeMask.DynamicGeometry : UnityEngine.Experimental.Rendering.RayTracingModeMask
UnityEngine.Experimental.Rendering.RayTracingModeMask.DynamicGeometry = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingModeMask.Everything : UnityEngine.Experimental.Rendering.RayTracingModeMask
UnityEngine.Experimental.Rendering.RayTracingModeMask.Everything = nil