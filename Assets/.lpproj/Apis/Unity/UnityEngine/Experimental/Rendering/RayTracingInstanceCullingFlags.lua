---@class UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags : System.Enum
UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags = {}

---@field public UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.value__ : System.Int32
UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.value__ = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.None : UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags
UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.None = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.EnableSphereCulling : UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags
UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.EnableSphereCulling = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.EnablePlaneCulling : UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags
UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.EnablePlaneCulling = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.EnableLODCulling : UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags
UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.EnableLODCulling = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.ComputeMaterialsCRC : UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags
UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.ComputeMaterialsCRC = nil

---@field public UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.IgnoreReflectionProbes : UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags
UnityEngine.Experimental.Rendering.RayTracingInstanceCullingFlags.IgnoreReflectionProbes = nil