---@class UnityEngine.Experimental.GlobalIllumination.Lightmapping : System.Object
UnityEngine.Experimental.GlobalIllumination.Lightmapping = {}

---@param del : UnityEngine.Experimental.GlobalIllumination.RequestLightsDelegate
function UnityEngine.Experimental.GlobalIllumination.Lightmapping.SetDelegate(del)
end

---@return UnityEngine.Experimental.GlobalIllumination.RequestLightsDelegate
function UnityEngine.Experimental.GlobalIllumination.Lightmapping.GetDelegate()
end

function UnityEngine.Experimental.GlobalIllumination.Lightmapping.ResetDelegate()
end