---@class UnityEngine.Experimental.GlobalIllumination.LinearColor : System.ValueType
UnityEngine.Experimental.GlobalIllumination.LinearColor = {}

---@property readwrite UnityEngine.Experimental.GlobalIllumination.LinearColor.red : System.Single
UnityEngine.Experimental.GlobalIllumination.LinearColor.red = nil

---@property readwrite UnityEngine.Experimental.GlobalIllumination.LinearColor.green : System.Single
UnityEngine.Experimental.GlobalIllumination.LinearColor.green = nil

---@property readwrite UnityEngine.Experimental.GlobalIllumination.LinearColor.blue : System.Single
UnityEngine.Experimental.GlobalIllumination.LinearColor.blue = nil

---@property readwrite UnityEngine.Experimental.GlobalIllumination.LinearColor.intensity : System.Single
UnityEngine.Experimental.GlobalIllumination.LinearColor.intensity = nil

---@param color : UnityEngine.Color
---@param intensity : System.Single
---@return UnityEngine.Experimental.GlobalIllumination.LinearColor
function UnityEngine.Experimental.GlobalIllumination.LinearColor.Convert(color, intensity)
end

---@return UnityEngine.Experimental.GlobalIllumination.LinearColor
function UnityEngine.Experimental.GlobalIllumination.LinearColor.Black()
end