---@class UnityEngine.ArticulationJacobian : System.ValueType
UnityEngine.ArticulationJacobian = {}

---@property readwrite UnityEngine.ArticulationJacobian.Item : System.Single
UnityEngine.ArticulationJacobian.Item = nil

---@property readwrite UnityEngine.ArticulationJacobian.rows : System.Int32
UnityEngine.ArticulationJacobian.rows = nil

---@property readwrite UnityEngine.ArticulationJacobian.columns : System.Int32
UnityEngine.ArticulationJacobian.columns = nil

---@property readwrite UnityEngine.ArticulationJacobian.elements : System.Collections.Generic.List
UnityEngine.ArticulationJacobian.elements = nil

---@param rows : System.Int32
---@param cols : System.Int32
---@return UnityEngine.ArticulationJacobian
function UnityEngine.ArticulationJacobian(rows, cols)
end