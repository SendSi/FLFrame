---@class UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute : System.Attribute
UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute = {}

---@property readonly UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute.labels : System.String[]
UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute.labels = nil

---@property readonly UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute.matchAll : System.Boolean
UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute.matchAll = nil

---@param labels : System.String[]
---@return UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute
function UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute(labels)
end

---@param matchAll : System.Boolean
---@param labels : System.String[]
---@return UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute
function UnityEngine.SearchService.ObjectSelectorHandlerWithLabelsAttribute(matchAll, labels)
end