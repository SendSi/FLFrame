---@class UnityEngine.ComputeBufferMode : System.Enum
UnityEngine.ComputeBufferMode = {}

---@field public UnityEngine.ComputeBufferMode.value__ : System.Int32
UnityEngine.ComputeBufferMode.value__ = nil

---@field public UnityEngine.ComputeBufferMode.Immutable : UnityEngine.ComputeBufferMode
UnityEngine.ComputeBufferMode.Immutable = nil

---@field public UnityEngine.ComputeBufferMode.Dynamic : UnityEngine.ComputeBufferMode
UnityEngine.ComputeBufferMode.Dynamic = nil

---@field public UnityEngine.ComputeBufferMode.Circular : UnityEngine.ComputeBufferMode
UnityEngine.ComputeBufferMode.Circular = nil

---@field public UnityEngine.ComputeBufferMode.StreamOut : UnityEngine.ComputeBufferMode
UnityEngine.ComputeBufferMode.StreamOut = nil

---@field public UnityEngine.ComputeBufferMode.SubUpdates : UnityEngine.ComputeBufferMode
UnityEngine.ComputeBufferMode.SubUpdates = nil