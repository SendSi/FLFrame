---@class UnityEngine.iPhoneTouch : System.ValueType
UnityEngine.iPhoneTouch = {}

---@property readonly UnityEngine.iPhoneTouch.positionDelta : UnityEngine.Vector2
UnityEngine.iPhoneTouch.positionDelta = nil

---@property readonly UnityEngine.iPhoneTouch.timeDelta : System.Single
UnityEngine.iPhoneTouch.timeDelta = nil

---@property readonly UnityEngine.iPhoneTouch.fingerId : System.Int32
UnityEngine.iPhoneTouch.fingerId = nil

---@property readonly UnityEngine.iPhoneTouch.position : UnityEngine.Vector2
UnityEngine.iPhoneTouch.position = nil

---@property readonly UnityEngine.iPhoneTouch.deltaPosition : UnityEngine.Vector2
UnityEngine.iPhoneTouch.deltaPosition = nil

---@property readonly UnityEngine.iPhoneTouch.deltaTime : System.Single
UnityEngine.iPhoneTouch.deltaTime = nil

---@property readonly UnityEngine.iPhoneTouch.tapCount : System.Int32
UnityEngine.iPhoneTouch.tapCount = nil

---@property readonly UnityEngine.iPhoneTouch.phase : UnityEngine.iPhoneTouchPhase
UnityEngine.iPhoneTouch.phase = nil