---@class UnityEngine.Scripting.RequiredMemberAttribute : System.Attribute
UnityEngine.Scripting.RequiredMemberAttribute = {}

---@return UnityEngine.Scripting.RequiredMemberAttribute
function UnityEngine.Scripting.RequiredMemberAttribute()
end