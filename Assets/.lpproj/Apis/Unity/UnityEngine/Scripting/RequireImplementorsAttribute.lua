---@class UnityEngine.Scripting.RequireImplementorsAttribute : System.Attribute
UnityEngine.Scripting.RequireImplementorsAttribute = {}

---@return UnityEngine.Scripting.RequireImplementorsAttribute
function UnityEngine.Scripting.RequireImplementorsAttribute()
end