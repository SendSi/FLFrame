---@class UnityEngine.Scripting.RequireDerivedAttribute : System.Attribute
UnityEngine.Scripting.RequireDerivedAttribute = {}

---@return UnityEngine.Scripting.RequireDerivedAttribute
function UnityEngine.Scripting.RequireDerivedAttribute()
end