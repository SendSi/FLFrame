---@class UnityEngine.ScrollWaitDefinitions : System.Object
UnityEngine.ScrollWaitDefinitions = {}

---@field public UnityEngine.ScrollWaitDefinitions.firstWait : System.Int32
UnityEngine.ScrollWaitDefinitions.firstWait = nil

---@field public UnityEngine.ScrollWaitDefinitions.regularWait : System.Int32
UnityEngine.ScrollWaitDefinitions.regularWait = nil