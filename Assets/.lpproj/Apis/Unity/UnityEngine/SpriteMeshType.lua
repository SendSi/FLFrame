---@class UnityEngine.SpriteMeshType : System.Enum
UnityEngine.SpriteMeshType = {}

---@field public UnityEngine.SpriteMeshType.value__ : System.Int32
UnityEngine.SpriteMeshType.value__ = nil

---@field public UnityEngine.SpriteMeshType.FullRect : UnityEngine.SpriteMeshType
UnityEngine.SpriteMeshType.FullRect = nil

---@field public UnityEngine.SpriteMeshType.Tight : UnityEngine.SpriteMeshType
UnityEngine.SpriteMeshType.Tight = nil