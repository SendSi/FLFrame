---@class UnityEngine.LightmappingMode : System.Enum
UnityEngine.LightmappingMode = {}

---@field public UnityEngine.LightmappingMode.value__ : System.Int32
UnityEngine.LightmappingMode.value__ = nil

---@field public UnityEngine.LightmappingMode.Realtime : UnityEngine.LightmappingMode
UnityEngine.LightmappingMode.Realtime = nil

---@field public UnityEngine.LightmappingMode.Baked : UnityEngine.LightmappingMode
UnityEngine.LightmappingMode.Baked = nil

---@field public UnityEngine.LightmappingMode.Mixed : UnityEngine.LightmappingMode
UnityEngine.LightmappingMode.Mixed = nil