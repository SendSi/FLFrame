---@class UnityEngine.AudioReverbFilter : UnityEngine.Behaviour
UnityEngine.AudioReverbFilter = {}

---@property readwrite UnityEngine.AudioReverbFilter.lFReference : System.Single
UnityEngine.AudioReverbFilter.lFReference = nil

---@property readwrite UnityEngine.AudioReverbFilter.reverbPreset : UnityEngine.AudioReverbPreset
UnityEngine.AudioReverbFilter.reverbPreset = nil

---@property readwrite UnityEngine.AudioReverbFilter.dryLevel : System.Single
UnityEngine.AudioReverbFilter.dryLevel = nil

---@property readwrite UnityEngine.AudioReverbFilter.room : System.Single
UnityEngine.AudioReverbFilter.room = nil

---@property readwrite UnityEngine.AudioReverbFilter.roomHF : System.Single
UnityEngine.AudioReverbFilter.roomHF = nil

---@property readwrite UnityEngine.AudioReverbFilter.roomRolloffFactor : System.Single
UnityEngine.AudioReverbFilter.roomRolloffFactor = nil

---@property readwrite UnityEngine.AudioReverbFilter.decayTime : System.Single
UnityEngine.AudioReverbFilter.decayTime = nil

---@property readwrite UnityEngine.AudioReverbFilter.decayHFRatio : System.Single
UnityEngine.AudioReverbFilter.decayHFRatio = nil

---@property readwrite UnityEngine.AudioReverbFilter.reflectionsLevel : System.Single
UnityEngine.AudioReverbFilter.reflectionsLevel = nil

---@property readwrite UnityEngine.AudioReverbFilter.reflectionsDelay : System.Single
UnityEngine.AudioReverbFilter.reflectionsDelay = nil

---@property readwrite UnityEngine.AudioReverbFilter.reverbLevel : System.Single
UnityEngine.AudioReverbFilter.reverbLevel = nil

---@property readwrite UnityEngine.AudioReverbFilter.reverbDelay : System.Single
UnityEngine.AudioReverbFilter.reverbDelay = nil

---@property readwrite UnityEngine.AudioReverbFilter.diffusion : System.Single
UnityEngine.AudioReverbFilter.diffusion = nil

---@property readwrite UnityEngine.AudioReverbFilter.density : System.Single
UnityEngine.AudioReverbFilter.density = nil

---@property readwrite UnityEngine.AudioReverbFilter.hfReference : System.Single
UnityEngine.AudioReverbFilter.hfReference = nil

---@property readwrite UnityEngine.AudioReverbFilter.roomLF : System.Single
UnityEngine.AudioReverbFilter.roomLF = nil

---@property readwrite UnityEngine.AudioReverbFilter.lfReference : System.Single
UnityEngine.AudioReverbFilter.lfReference = nil

---@return UnityEngine.AudioReverbFilter
function UnityEngine.AudioReverbFilter()
end