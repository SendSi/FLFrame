---@class UnityEngine.SpriteSortPoint : System.Enum
UnityEngine.SpriteSortPoint = {}

---@field public UnityEngine.SpriteSortPoint.value__ : System.Int32
UnityEngine.SpriteSortPoint.value__ = nil

---@field public UnityEngine.SpriteSortPoint.Center : UnityEngine.SpriteSortPoint
UnityEngine.SpriteSortPoint.Center = nil

---@field public UnityEngine.SpriteSortPoint.Pivot : UnityEngine.SpriteSortPoint
UnityEngine.SpriteSortPoint.Pivot = nil