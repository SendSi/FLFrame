---@class UnityEngine.UI.SpriteState : System.ValueType
UnityEngine.UI.SpriteState = {}

---@property readwrite UnityEngine.UI.SpriteState.highlightedSprite : UnityEngine.Sprite
UnityEngine.UI.SpriteState.highlightedSprite = nil

---@property readwrite UnityEngine.UI.SpriteState.pressedSprite : UnityEngine.Sprite
UnityEngine.UI.SpriteState.pressedSprite = nil

---@property readwrite UnityEngine.UI.SpriteState.selectedSprite : UnityEngine.Sprite
UnityEngine.UI.SpriteState.selectedSprite = nil

---@property readwrite UnityEngine.UI.SpriteState.disabledSprite : UnityEngine.Sprite
UnityEngine.UI.SpriteState.disabledSprite = nil

---@param other : UnityEngine.UI.SpriteState
---@return System.Boolean
function UnityEngine.UI.SpriteState:Equals(other)
end