---@class UnityEngine.UI.ILayoutElement
UnityEngine.UI.ILayoutElement = {}

---@property readonly UnityEngine.UI.ILayoutElement.minWidth : System.Single
UnityEngine.UI.ILayoutElement.minWidth = nil

---@property readonly UnityEngine.UI.ILayoutElement.preferredWidth : System.Single
UnityEngine.UI.ILayoutElement.preferredWidth = nil

---@property readonly UnityEngine.UI.ILayoutElement.flexibleWidth : System.Single
UnityEngine.UI.ILayoutElement.flexibleWidth = nil

---@property readonly UnityEngine.UI.ILayoutElement.minHeight : System.Single
UnityEngine.UI.ILayoutElement.minHeight = nil

---@property readonly UnityEngine.UI.ILayoutElement.preferredHeight : System.Single
UnityEngine.UI.ILayoutElement.preferredHeight = nil

---@property readonly UnityEngine.UI.ILayoutElement.flexibleHeight : System.Single
UnityEngine.UI.ILayoutElement.flexibleHeight = nil

---@property readonly UnityEngine.UI.ILayoutElement.layoutPriority : System.Int32
UnityEngine.UI.ILayoutElement.layoutPriority = nil

function UnityEngine.UI.ILayoutElement:CalculateLayoutInputHorizontal()
end

function UnityEngine.UI.ILayoutElement:CalculateLayoutInputVertical()
end