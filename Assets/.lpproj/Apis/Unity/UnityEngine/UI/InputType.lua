---@class UnityEngine.UI.InputType : System.Enum
UnityEngine.UI.InputType = {}

---@field public UnityEngine.UI.InputType.value__ : System.Int32
UnityEngine.UI.InputType.value__ = nil

---@field public UnityEngine.UI.InputType.Standard : UnityEngine.UI.InputType
UnityEngine.UI.InputType.Standard = nil

---@field public UnityEngine.UI.InputType.AutoCorrect : UnityEngine.UI.InputType
UnityEngine.UI.InputType.AutoCorrect = nil

---@field public UnityEngine.UI.InputType.Password : UnityEngine.UI.InputType
UnityEngine.UI.InputType.Password = nil