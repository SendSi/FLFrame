---@class UnityEngine.UI.RectangularVertexClipper : System.Object
UnityEngine.UI.RectangularVertexClipper = {}

---@return UnityEngine.UI.RectangularVertexClipper
function UnityEngine.UI.RectangularVertexClipper()
end

---@param t : UnityEngine.RectTransform
---@param c : UnityEngine.Canvas
---@return UnityEngine.Rect
function UnityEngine.UI.RectangularVertexClipper:GetCanvasRect(t, c)
end