---@class UnityEngine.UI.Misc : System.Object
UnityEngine.UI.Misc = {}

---@param obj : UnityEngine.Object
function UnityEngine.UI.Misc.Destroy(obj)
end

---@param obj : UnityEngine.Object
function UnityEngine.UI.Misc.DestroyImmediate(obj)
end