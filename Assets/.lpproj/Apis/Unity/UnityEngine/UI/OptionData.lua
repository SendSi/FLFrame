---@class UnityEngine.UI.OptionData : System.Object
UnityEngine.UI.OptionData = {}

---@property readwrite UnityEngine.UI.OptionData.text : System.String
UnityEngine.UI.OptionData.text = nil

---@property readwrite UnityEngine.UI.OptionData.image : UnityEngine.Sprite
UnityEngine.UI.OptionData.image = nil

---@return UnityEngine.UI.OptionData
function UnityEngine.UI.OptionData()
end

---@param text : System.String
---@return UnityEngine.UI.OptionData
function UnityEngine.UI.OptionData(text)
end

---@param image : UnityEngine.Sprite
---@return UnityEngine.UI.OptionData
function UnityEngine.UI.OptionData(image)
end

---@param text : System.String
---@param image : UnityEngine.Sprite
---@return UnityEngine.UI.OptionData
function UnityEngine.UI.OptionData(text, image)
end