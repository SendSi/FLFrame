---@class UnityEngine.AddComponentMenu : System.Attribute
UnityEngine.AddComponentMenu = {}

---@property readonly UnityEngine.AddComponentMenu.componentMenu : System.String
UnityEngine.AddComponentMenu.componentMenu = nil

---@property readonly UnityEngine.AddComponentMenu.componentOrder : System.Int32
UnityEngine.AddComponentMenu.componentOrder = nil

---@param menuName : System.String
---@return UnityEngine.AddComponentMenu
function UnityEngine.AddComponentMenu(menuName)
end

---@param menuName : System.String
---@param order : System.Int32
---@return UnityEngine.AddComponentMenu
function UnityEngine.AddComponentMenu(menuName, order)
end