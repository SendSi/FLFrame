---@class UnityEngine.RenderTextureReadWrite : System.Enum
UnityEngine.RenderTextureReadWrite = {}

---@field public UnityEngine.RenderTextureReadWrite.value__ : System.Int32
UnityEngine.RenderTextureReadWrite.value__ = nil

---@field public UnityEngine.RenderTextureReadWrite.Default : UnityEngine.RenderTextureReadWrite
UnityEngine.RenderTextureReadWrite.Default = nil

---@field public UnityEngine.RenderTextureReadWrite.Linear : UnityEngine.RenderTextureReadWrite
UnityEngine.RenderTextureReadWrite.Linear = nil

---@field public UnityEngine.RenderTextureReadWrite.sRGB : UnityEngine.RenderTextureReadWrite
UnityEngine.RenderTextureReadWrite.sRGB = nil