---@class UnityEngine.TextureFormat : System.Enum
UnityEngine.TextureFormat = {}

---@field public UnityEngine.TextureFormat.value__ : System.Int32
UnityEngine.TextureFormat.value__ = nil

---@field public UnityEngine.TextureFormat.Alpha8 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.Alpha8 = nil

---@field public UnityEngine.TextureFormat.ARGB4444 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ARGB4444 = nil

---@field public UnityEngine.TextureFormat.RGB24 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGB24 = nil

---@field public UnityEngine.TextureFormat.RGBA32 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGBA32 = nil

---@field public UnityEngine.TextureFormat.ARGB32 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ARGB32 = nil

---@field public UnityEngine.TextureFormat.RGB565 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGB565 = nil

---@field public UnityEngine.TextureFormat.R16 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.R16 = nil

---@field public UnityEngine.TextureFormat.DXT1 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.DXT1 = nil

---@field public UnityEngine.TextureFormat.DXT5 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.DXT5 = nil

---@field public UnityEngine.TextureFormat.RGBA4444 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGBA4444 = nil

---@field public UnityEngine.TextureFormat.BGRA32 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.BGRA32 = nil

---@field public UnityEngine.TextureFormat.RHalf : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RHalf = nil

---@field public UnityEngine.TextureFormat.RGHalf : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGHalf = nil

---@field public UnityEngine.TextureFormat.RGBAHalf : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGBAHalf = nil

---@field public UnityEngine.TextureFormat.RFloat : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RFloat = nil

---@field public UnityEngine.TextureFormat.RGFloat : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGFloat = nil

---@field public UnityEngine.TextureFormat.RGBAFloat : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGBAFloat = nil

---@field public UnityEngine.TextureFormat.YUY2 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.YUY2 = nil

---@field public UnityEngine.TextureFormat.RGB9e5Float : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGB9e5Float = nil

---@field public UnityEngine.TextureFormat.BC4 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.BC4 = nil

---@field public UnityEngine.TextureFormat.BC5 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.BC5 = nil

---@field public UnityEngine.TextureFormat.BC6H : UnityEngine.TextureFormat
UnityEngine.TextureFormat.BC6H = nil

---@field public UnityEngine.TextureFormat.BC7 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.BC7 = nil

---@field public UnityEngine.TextureFormat.DXT1Crunched : UnityEngine.TextureFormat
UnityEngine.TextureFormat.DXT1Crunched = nil

---@field public UnityEngine.TextureFormat.DXT5Crunched : UnityEngine.TextureFormat
UnityEngine.TextureFormat.DXT5Crunched = nil

---@field public UnityEngine.TextureFormat.PVRTC_RGB2 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.PVRTC_RGB2 = nil

---@field public UnityEngine.TextureFormat.PVRTC_RGBA2 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.PVRTC_RGBA2 = nil

---@field public UnityEngine.TextureFormat.PVRTC_RGB4 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.PVRTC_RGB4 = nil

---@field public UnityEngine.TextureFormat.PVRTC_RGBA4 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.PVRTC_RGBA4 = nil

---@field public UnityEngine.TextureFormat.ETC_RGB4 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ETC_RGB4 = nil

---@field public UnityEngine.TextureFormat.ATC_RGB4 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ATC_RGB4 = nil

---@field public UnityEngine.TextureFormat.ATC_RGBA8 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ATC_RGBA8 = nil

---@field public UnityEngine.TextureFormat.EAC_R : UnityEngine.TextureFormat
UnityEngine.TextureFormat.EAC_R = nil

---@field public UnityEngine.TextureFormat.EAC_R_SIGNED : UnityEngine.TextureFormat
UnityEngine.TextureFormat.EAC_R_SIGNED = nil

---@field public UnityEngine.TextureFormat.EAC_RG : UnityEngine.TextureFormat
UnityEngine.TextureFormat.EAC_RG = nil

---@field public UnityEngine.TextureFormat.EAC_RG_SIGNED : UnityEngine.TextureFormat
UnityEngine.TextureFormat.EAC_RG_SIGNED = nil

---@field public UnityEngine.TextureFormat.ETC2_RGB : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ETC2_RGB = nil

---@field public UnityEngine.TextureFormat.ETC2_RGBA1 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ETC2_RGBA1 = nil

---@field public UnityEngine.TextureFormat.ETC2_RGBA8 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ETC2_RGBA8 = nil

---@field public UnityEngine.TextureFormat.ASTC_4x4 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_4x4 = nil

---@field public UnityEngine.TextureFormat.ASTC_5x5 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_5x5 = nil

---@field public UnityEngine.TextureFormat.ASTC_6x6 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_6x6 = nil

---@field public UnityEngine.TextureFormat.ASTC_8x8 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_8x8 = nil

---@field public UnityEngine.TextureFormat.ASTC_10x10 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_10x10 = nil

---@field public UnityEngine.TextureFormat.ASTC_12x12 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_12x12 = nil

---@field public UnityEngine.TextureFormat.ETC_RGB4_3DS : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ETC_RGB4_3DS = nil

---@field public UnityEngine.TextureFormat.ETC_RGBA8_3DS : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ETC_RGBA8_3DS = nil

---@field public UnityEngine.TextureFormat.RG16 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RG16 = nil

---@field public UnityEngine.TextureFormat.R8 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.R8 = nil

---@field public UnityEngine.TextureFormat.ETC_RGB4Crunched : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ETC_RGB4Crunched = nil

---@field public UnityEngine.TextureFormat.ETC2_RGBA8Crunched : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ETC2_RGBA8Crunched = nil

---@field public UnityEngine.TextureFormat.ASTC_HDR_4x4 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_HDR_4x4 = nil

---@field public UnityEngine.TextureFormat.ASTC_HDR_5x5 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_HDR_5x5 = nil

---@field public UnityEngine.TextureFormat.ASTC_HDR_6x6 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_HDR_6x6 = nil

---@field public UnityEngine.TextureFormat.ASTC_HDR_8x8 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_HDR_8x8 = nil

---@field public UnityEngine.TextureFormat.ASTC_HDR_10x10 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_HDR_10x10 = nil

---@field public UnityEngine.TextureFormat.ASTC_HDR_12x12 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_HDR_12x12 = nil

---@field public UnityEngine.TextureFormat.RG32 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RG32 = nil

---@field public UnityEngine.TextureFormat.RGB48 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGB48 = nil

---@field public UnityEngine.TextureFormat.RGBA64 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.RGBA64 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGB_4x4 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGB_4x4 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGB_5x5 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGB_5x5 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGB_6x6 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGB_6x6 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGB_8x8 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGB_8x8 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGB_10x10 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGB_10x10 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGB_12x12 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGB_12x12 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGBA_4x4 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGBA_4x4 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGBA_5x5 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGBA_5x5 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGBA_6x6 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGBA_6x6 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGBA_8x8 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGBA_8x8 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGBA_10x10 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGBA_10x10 = nil

---@field public UnityEngine.TextureFormat.ASTC_RGBA_12x12 : UnityEngine.TextureFormat
UnityEngine.TextureFormat.ASTC_RGBA_12x12 = nil

---@field public UnityEngine.TextureFormat.PVRTC_2BPP_RGB : UnityEngine.TextureFormat
UnityEngine.TextureFormat.PVRTC_2BPP_RGB = nil

---@field public UnityEngine.TextureFormat.PVRTC_2BPP_RGBA : UnityEngine.TextureFormat
UnityEngine.TextureFormat.PVRTC_2BPP_RGBA = nil

---@field public UnityEngine.TextureFormat.PVRTC_4BPP_RGB : UnityEngine.TextureFormat
UnityEngine.TextureFormat.PVRTC_4BPP_RGB = nil

---@field public UnityEngine.TextureFormat.PVRTC_4BPP_RGBA : UnityEngine.TextureFormat
UnityEngine.TextureFormat.PVRTC_4BPP_RGBA = nil