---@class FairyGUI.GearAnimationValue : System.Object
FairyGUI.GearAnimationValue = {}

---@field public FairyGUI.GearAnimationValue.playing : System.Boolean
FairyGUI.GearAnimationValue.playing = nil

---@field public FairyGUI.GearAnimationValue.frame : System.Int32
FairyGUI.GearAnimationValue.frame = nil

---@param playing : System.Boolean
---@param frame : System.Int32
---@return FairyGUI.GearAnimationValue
function FairyGUI.GearAnimationValue(playing, frame)
end