---@class FairyGUI.TValue_Shake : System.Object
FairyGUI.TValue_Shake = {}

---@field public FairyGUI.TValue_Shake.amplitude : System.Single
FairyGUI.TValue_Shake.amplitude = nil

---@field public FairyGUI.TValue_Shake.duration : System.Single
FairyGUI.TValue_Shake.duration = nil

---@field public FairyGUI.TValue_Shake.lastOffset : UnityEngine.Vector2
FairyGUI.TValue_Shake.lastOffset = nil

---@field public FairyGUI.TValue_Shake.offset : UnityEngine.Vector2
FairyGUI.TValue_Shake.offset = nil

---@return FairyGUI.TValue_Shake
function FairyGUI.TValue_Shake()
end