---@class FairyGUI.Origin90 : System.Enum
FairyGUI.Origin90 = {}

---@field public FairyGUI.Origin90.value__ : System.Int32
FairyGUI.Origin90.value__ = nil

---@field public FairyGUI.Origin90.TopLeft : FairyGUI.Origin90
FairyGUI.Origin90.TopLeft = nil

---@field public FairyGUI.Origin90.TopRight : FairyGUI.Origin90
FairyGUI.Origin90.TopRight = nil

---@field public FairyGUI.Origin90.BottomLeft : FairyGUI.Origin90
FairyGUI.Origin90.BottomLeft = nil

---@field public FairyGUI.Origin90.BottomRight : FairyGUI.Origin90
FairyGUI.Origin90.BottomRight = nil