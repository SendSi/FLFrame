---@class FairyGUI.ButtonMode : System.Enum
FairyGUI.ButtonMode = {}

---@field public FairyGUI.ButtonMode.value__ : System.Int32
FairyGUI.ButtonMode.value__ = nil

---@field public FairyGUI.ButtonMode.Common : FairyGUI.ButtonMode
FairyGUI.ButtonMode.Common = nil

---@field public FairyGUI.ButtonMode.Check : FairyGUI.ButtonMode
FairyGUI.ButtonMode.Check = nil

---@field public FairyGUI.ButtonMode.Radio : FairyGUI.ButtonMode
FairyGUI.ButtonMode.Radio = nil