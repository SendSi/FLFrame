---@class FairyGUI.AsyncCreationHelper : System.Object
FairyGUI.AsyncCreationHelper = {}

---@return FairyGUI.AsyncCreationHelper
function FairyGUI.AsyncCreationHelper()
end

---@param item : FairyGUI.PackageItem
---@param callback : FairyGUI.CreateObjectCallback
function FairyGUI.AsyncCreationHelper.CreateObject(item, callback)
end