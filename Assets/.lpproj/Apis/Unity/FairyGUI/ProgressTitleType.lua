---@class FairyGUI.ProgressTitleType : System.Enum
FairyGUI.ProgressTitleType = {}

---@field public FairyGUI.ProgressTitleType.value__ : System.Int32
FairyGUI.ProgressTitleType.value__ = nil

---@field public FairyGUI.ProgressTitleType.Percent : FairyGUI.ProgressTitleType
FairyGUI.ProgressTitleType.Percent = nil

---@field public FairyGUI.ProgressTitleType.ValueAndMax : FairyGUI.ProgressTitleType
FairyGUI.ProgressTitleType.ValueAndMax = nil

---@field public FairyGUI.ProgressTitleType.Value : FairyGUI.ProgressTitleType
FairyGUI.ProgressTitleType.Value = nil

---@field public FairyGUI.ProgressTitleType.Max : FairyGUI.ProgressTitleType
FairyGUI.ProgressTitleType.Max = nil