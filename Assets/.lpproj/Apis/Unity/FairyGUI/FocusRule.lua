---@class FairyGUI.FocusRule : System.Enum
FairyGUI.FocusRule = {}

---@field public FairyGUI.FocusRule.value__ : System.Int32
FairyGUI.FocusRule.value__ = nil

---@field public FairyGUI.FocusRule.NotFocusable : FairyGUI.FocusRule
FairyGUI.FocusRule.NotFocusable = nil

---@field public FairyGUI.FocusRule.Focusable : FairyGUI.FocusRule
FairyGUI.FocusRule.Focusable = nil

---@field public FairyGUI.FocusRule.NavigationBase : FairyGUI.FocusRule
FairyGUI.FocusRule.NavigationBase = nil