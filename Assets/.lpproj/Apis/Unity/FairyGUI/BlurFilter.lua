---@class FairyGUI.BlurFilter : System.Object
FairyGUI.BlurFilter = {}

---@field public FairyGUI.BlurFilter.blurSize : System.Single
FairyGUI.BlurFilter.blurSize = nil

---@property readwrite FairyGUI.BlurFilter.target : FairyGUI.DisplayObject
FairyGUI.BlurFilter.target = nil

---@return FairyGUI.BlurFilter
function FairyGUI.BlurFilter()
end

function FairyGUI.BlurFilter:Dispose()
end

function FairyGUI.BlurFilter:Update()
end