---@class FairyGUI.StencilEraser : System.Object
FairyGUI.StencilEraser = {}

---@field public FairyGUI.StencilEraser.gameObject : UnityEngine.GameObject
FairyGUI.StencilEraser.gameObject = nil

---@field public FairyGUI.StencilEraser.meshFilter : UnityEngine.MeshFilter
FairyGUI.StencilEraser.meshFilter = nil

---@field public FairyGUI.StencilEraser.meshRenderer : UnityEngine.MeshRenderer
FairyGUI.StencilEraser.meshRenderer = nil

---@property readwrite FairyGUI.StencilEraser.enabled : System.Boolean
FairyGUI.StencilEraser.enabled = nil

---@param parent : UnityEngine.Transform
---@return FairyGUI.StencilEraser
function FairyGUI.StencilEraser(parent)
end