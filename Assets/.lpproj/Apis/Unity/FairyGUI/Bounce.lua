---@class FairyGUI.Bounce : System.Object
FairyGUI.Bounce = {}

---@param time : System.Single
---@param duration : System.Single
---@return System.Single
function FairyGUI.Bounce.EaseIn(time, duration)
end

---@param time : System.Single
---@param duration : System.Single
---@return System.Single
function FairyGUI.Bounce.EaseOut(time, duration)
end

---@param time : System.Single
---@param duration : System.Single
---@return System.Single
function FairyGUI.Bounce.EaseInOut(time, duration)
end