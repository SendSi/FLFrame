---@class FairyGUI.GearAnimation : FairyGUI.GearBase
FairyGUI.GearAnimation = {}

---@param owner : FairyGUI.GObject
---@return FairyGUI.GearAnimation
function FairyGUI.GearAnimation(owner)
end

function FairyGUI.GearAnimation:Apply()
end

function FairyGUI.GearAnimation:UpdateState()
end