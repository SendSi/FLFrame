---@class FairyGUI.VertAlignType : System.Enum
FairyGUI.VertAlignType = {}

---@field public FairyGUI.VertAlignType.value__ : System.Int32
FairyGUI.VertAlignType.value__ = nil

---@field public FairyGUI.VertAlignType.Top : FairyGUI.VertAlignType
FairyGUI.VertAlignType.Top = nil

---@field public FairyGUI.VertAlignType.Middle : FairyGUI.VertAlignType
FairyGUI.VertAlignType.Middle = nil

---@field public FairyGUI.VertAlignType.Bottom : FairyGUI.VertAlignType
FairyGUI.VertAlignType.Bottom = nil