---@class FairyGUI.AlignType : System.Enum
FairyGUI.AlignType = {}

---@field public FairyGUI.AlignType.value__ : System.Int32
FairyGUI.AlignType.value__ = nil

---@field public FairyGUI.AlignType.Left : FairyGUI.AlignType
FairyGUI.AlignType.Left = nil

---@field public FairyGUI.AlignType.Center : FairyGUI.AlignType
FairyGUI.AlignType.Center = nil

---@field public FairyGUI.AlignType.Right : FairyGUI.AlignType
FairyGUI.AlignType.Right = nil