---@class FairyGUI.TweenValue : System.Object
FairyGUI.TweenValue = {}

---@field public FairyGUI.TweenValue.x : System.Single
FairyGUI.TweenValue.x = nil

---@field public FairyGUI.TweenValue.y : System.Single
FairyGUI.TweenValue.y = nil

---@field public FairyGUI.TweenValue.z : System.Single
FairyGUI.TweenValue.z = nil

---@field public FairyGUI.TweenValue.w : System.Single
FairyGUI.TweenValue.w = nil

---@field public FairyGUI.TweenValue.d : System.Double
FairyGUI.TweenValue.d = nil

---@property readwrite FairyGUI.TweenValue.vec2 : UnityEngine.Vector2
FairyGUI.TweenValue.vec2 = nil

---@property readwrite FairyGUI.TweenValue.vec3 : UnityEngine.Vector3
FairyGUI.TweenValue.vec3 = nil

---@property readwrite FairyGUI.TweenValue.vec4 : UnityEngine.Vector4
FairyGUI.TweenValue.vec4 = nil

---@property readwrite FairyGUI.TweenValue.color : UnityEngine.Color
FairyGUI.TweenValue.color = nil

---@property readwrite FairyGUI.TweenValue.Item : System.Single
FairyGUI.TweenValue.Item = nil

---@return FairyGUI.TweenValue
function FairyGUI.TweenValue()
end

function FairyGUI.TweenValue:SetZero()
end