---@class FairyGUI.IColorGear
FairyGUI.IColorGear = {}

---@property readwrite FairyGUI.IColorGear.color : UnityEngine.Color
FairyGUI.IColorGear.color = nil