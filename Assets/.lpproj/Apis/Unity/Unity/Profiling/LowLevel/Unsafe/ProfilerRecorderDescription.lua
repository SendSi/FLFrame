---@module Unity.Profiling.LowLevel.Unsafe
Unity.Profiling.LowLevel.Unsafe = {}

---@class Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription : System.ValueType
Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription = {}

---@property readonly Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.Category : Unity.Profiling.ProfilerCategory
Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.Category = nil

---@property readonly Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.Flags : Unity.Profiling.LowLevel.MarkerFlags
Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.Flags = nil

---@property readonly Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.DataType : Unity.Profiling.LowLevel.ProfilerMarkerDataType
Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.DataType = nil

---@property readonly Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.UnitType : Unity.Profiling.ProfilerMarkerDataUnit
Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.UnitType = nil

---@property readonly Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.NameUtf8Len : System.Int32
Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.NameUtf8Len = nil

---@property readonly Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.NameUtf8 : System.BytePointer
Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.NameUtf8 = nil

---@property readonly Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.Name : System.String
Unity.Profiling.LowLevel.Unsafe.ProfilerRecorderDescription.Name = nil