---@class Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription : System.ValueType
Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription = {}

---@field public Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.Id : System.UInt16
Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.Id = nil

---@field public Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.Flags : System.UInt16
Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.Flags = nil

---@field public Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.Color : UnityEngine.Color32
Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.Color = nil

---@field public Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.NameUtf8Len : System.Int32
Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.NameUtf8Len = nil

---@field public Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.NameUtf8 : System.BytePointer
Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.NameUtf8 = nil

---@property readonly Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.Name : System.String
Unity.Profiling.LowLevel.Unsafe.ProfilerCategoryDescription.Name = nil