---@class Unity.Profiling.ProfilerCounterOptions : System.Enum
Unity.Profiling.ProfilerCounterOptions = {}

---@field public Unity.Profiling.ProfilerCounterOptions.value__ : System.UInt16
Unity.Profiling.ProfilerCounterOptions.value__ = nil

---@field public Unity.Profiling.ProfilerCounterOptions.None : Unity.Profiling.ProfilerCounterOptions
Unity.Profiling.ProfilerCounterOptions.None = nil

---@field public Unity.Profiling.ProfilerCounterOptions.FlushOnEndOfFrame : Unity.Profiling.ProfilerCounterOptions
Unity.Profiling.ProfilerCounterOptions.FlushOnEndOfFrame = nil

---@field public Unity.Profiling.ProfilerCounterOptions.ResetToZeroOnFlush : Unity.Profiling.ProfilerCounterOptions
Unity.Profiling.ProfilerCounterOptions.ResetToZeroOnFlush = nil