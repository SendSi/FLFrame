---@class Unity.Collections.NativeDisableParallelForRestrictionAttribute : System.Attribute
Unity.Collections.NativeDisableParallelForRestrictionAttribute = {}

---@return Unity.Collections.NativeDisableParallelForRestrictionAttribute
function Unity.Collections.NativeDisableParallelForRestrictionAttribute()
end