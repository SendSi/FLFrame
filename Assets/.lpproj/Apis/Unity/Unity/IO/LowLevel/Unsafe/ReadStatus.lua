---@class Unity.IO.LowLevel.Unsafe.ReadStatus : System.Enum
Unity.IO.LowLevel.Unsafe.ReadStatus = {}

---@field public Unity.IO.LowLevel.Unsafe.ReadStatus.value__ : System.Int32
Unity.IO.LowLevel.Unsafe.ReadStatus.value__ = nil

---@field public Unity.IO.LowLevel.Unsafe.ReadStatus.Complete : Unity.IO.LowLevel.Unsafe.ReadStatus
Unity.IO.LowLevel.Unsafe.ReadStatus.Complete = nil

---@field public Unity.IO.LowLevel.Unsafe.ReadStatus.InProgress : Unity.IO.LowLevel.Unsafe.ReadStatus
Unity.IO.LowLevel.Unsafe.ReadStatus.InProgress = nil

---@field public Unity.IO.LowLevel.Unsafe.ReadStatus.Failed : Unity.IO.LowLevel.Unsafe.ReadStatus
Unity.IO.LowLevel.Unsafe.ReadStatus.Failed = nil

---@field public Unity.IO.LowLevel.Unsafe.ReadStatus.Truncated : Unity.IO.LowLevel.Unsafe.ReadStatus
Unity.IO.LowLevel.Unsafe.ReadStatus.Truncated = nil

---@field public Unity.IO.LowLevel.Unsafe.ReadStatus.Canceled : Unity.IO.LowLevel.Unsafe.ReadStatus
Unity.IO.LowLevel.Unsafe.ReadStatus.Canceled = nil