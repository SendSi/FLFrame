---@class Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters : System.Object
Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters = {}

---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters()
end

---@param typeID : System.UInt64
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(typeID)
end

---@param state : Unity.IO.LowLevel.Unsafe.ProcessingState
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(state)
end

---@param readType : Unity.IO.LowLevel.Unsafe.FileReadType
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(readType)
end

---@param priorityLevel : Unity.IO.LowLevel.Unsafe.Priority
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(priorityLevel)
end

---@param subsystem : Unity.IO.LowLevel.Unsafe.AssetLoadingSubsystem
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(subsystem)
end

---@param typeIDs : System.UInt64[]
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(typeIDs)
end

---@param states : Unity.IO.LowLevel.Unsafe.ProcessingState[]
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(states)
end

---@param readTypes : Unity.IO.LowLevel.Unsafe.FileReadType[]
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(readTypes)
end

---@param priorityLevels : Unity.IO.LowLevel.Unsafe.Priority[]
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(priorityLevels)
end

---@param subsystems : Unity.IO.LowLevel.Unsafe.AssetLoadingSubsystem[]
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(subsystems)
end

---@param typeIDs : System.UInt64[]
---@param states : Unity.IO.LowLevel.Unsafe.ProcessingState[]
---@param readTypes : Unity.IO.LowLevel.Unsafe.FileReadType[]
---@param priorityLevels : Unity.IO.LowLevel.Unsafe.Priority[]
---@param subsystems : Unity.IO.LowLevel.Unsafe.AssetLoadingSubsystem[]
---@return Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters(typeIDs, states, readTypes, priorityLevels, subsystems)
end

---@param _typeIDs : System.UInt64[]
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetTypeIDFilter(_typeIDs)
end

---@param _states : Unity.IO.LowLevel.Unsafe.ProcessingState[]
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetStateFilter(_states)
end

---@param _readTypes : Unity.IO.LowLevel.Unsafe.FileReadType[]
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetReadTypeFilter(_readTypes)
end

---@param _priorityLevels : Unity.IO.LowLevel.Unsafe.Priority[]
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetPriorityFilter(_priorityLevels)
end

---@param _subsystems : Unity.IO.LowLevel.Unsafe.AssetLoadingSubsystem[]
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetSubsystemFilter(_subsystems)
end

---@param _typeID : System.UInt64
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetTypeIDFilter(_typeID)
end

---@param _state : Unity.IO.LowLevel.Unsafe.ProcessingState
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetStateFilter(_state)
end

---@param _readType : Unity.IO.LowLevel.Unsafe.FileReadType
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetReadTypeFilter(_readType)
end

---@param _priorityLevel : Unity.IO.LowLevel.Unsafe.Priority
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetPriorityFilter(_priorityLevel)
end

---@param _subsystem : Unity.IO.LowLevel.Unsafe.AssetLoadingSubsystem
function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:SetSubsystemFilter(_subsystem)
end

function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:RemoveTypeIDFilter()
end

function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:RemoveStateFilter()
end

function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:RemoveReadTypeFilter()
end

function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:RemovePriorityFilter()
end

function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:RemoveSubsystemFilter()
end

function Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetricsFilters:ClearFilters()
end