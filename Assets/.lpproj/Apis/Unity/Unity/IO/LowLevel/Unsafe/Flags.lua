---@class Unity.IO.LowLevel.Unsafe.Flags : System.Enum
Unity.IO.LowLevel.Unsafe.Flags = {}

---@field public Unity.IO.LowLevel.Unsafe.Flags.value__ : System.Int32
Unity.IO.LowLevel.Unsafe.Flags.value__ = nil

---@field public Unity.IO.LowLevel.Unsafe.Flags.None : Unity.IO.LowLevel.Unsafe.Flags
Unity.IO.LowLevel.Unsafe.Flags.None = nil

---@field public Unity.IO.LowLevel.Unsafe.Flags.ClearOnRead : Unity.IO.LowLevel.Unsafe.Flags
Unity.IO.LowLevel.Unsafe.Flags.ClearOnRead = nil