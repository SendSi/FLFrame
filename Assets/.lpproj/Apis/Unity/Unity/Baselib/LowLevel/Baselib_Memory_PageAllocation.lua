---@class Unity.Baselib.LowLevel.Baselib_Memory_PageAllocation : System.ValueType
Unity.Baselib.LowLevel.Baselib_Memory_PageAllocation = {}

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageAllocation.ptr : System.IntPtr
Unity.Baselib.LowLevel.Baselib_Memory_PageAllocation.ptr = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageAllocation.pageSize : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageAllocation.pageSize = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageAllocation.pageCount : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageAllocation.pageCount = nil