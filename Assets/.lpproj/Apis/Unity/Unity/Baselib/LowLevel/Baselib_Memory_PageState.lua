---@class Unity.Baselib.LowLevel.Baselib_Memory_PageState : System.Enum
Unity.Baselib.LowLevel.Baselib_Memory_PageState = {}

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageState.value__ : System.Int32
Unity.Baselib.LowLevel.Baselib_Memory_PageState.value__ = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageState.Reserved : Unity.Baselib.LowLevel.Baselib_Memory_PageState
Unity.Baselib.LowLevel.Baselib_Memory_PageState.Reserved = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageState.NoAccess : Unity.Baselib.LowLevel.Baselib_Memory_PageState
Unity.Baselib.LowLevel.Baselib_Memory_PageState.NoAccess = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageState.ReadOnly : Unity.Baselib.LowLevel.Baselib_Memory_PageState
Unity.Baselib.LowLevel.Baselib_Memory_PageState.ReadOnly = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageState.ReadWrite : Unity.Baselib.LowLevel.Baselib_Memory_PageState
Unity.Baselib.LowLevel.Baselib_Memory_PageState.ReadWrite = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageState.ReadOnly_Executable : Unity.Baselib.LowLevel.Baselib_Memory_PageState
Unity.Baselib.LowLevel.Baselib_Memory_PageState.ReadOnly_Executable = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageState.ReadWrite_Executable : Unity.Baselib.LowLevel.Baselib_Memory_PageState
Unity.Baselib.LowLevel.Baselib_Memory_PageState.ReadWrite_Executable = nil