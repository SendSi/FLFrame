---@class Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo : System.ValueType
Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo = {}

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.defaultPageSize : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.defaultPageSize = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes0 : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes0 = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes1 : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes1 = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes2 : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes2 = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes3 : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes3 = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes4 : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes4 = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes5 : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizes5 = nil

---@field public Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizesLen : System.UInt64
Unity.Baselib.LowLevel.Baselib_Memory_PageSizeInfo.pageSizesLen = nil