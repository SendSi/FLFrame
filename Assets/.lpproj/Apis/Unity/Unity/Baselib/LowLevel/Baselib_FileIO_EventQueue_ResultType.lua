---@class Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType : System.Enum
Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType = {}

---@field public Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.value__ : System.Int32
Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.value__ = nil

---@field public Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.Baselib_FileIO_EventQueue_Callback : Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType
Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.Baselib_FileIO_EventQueue_Callback = nil

---@field public Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.Baselib_FileIO_EventQueue_OpenFile : Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType
Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.Baselib_FileIO_EventQueue_OpenFile = nil

---@field public Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.Baselib_FileIO_EventQueue_ReadFile : Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType
Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.Baselib_FileIO_EventQueue_ReadFile = nil

---@field public Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.Baselib_FileIO_EventQueue_CloseFile : Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType
Unity.Baselib.LowLevel.Baselib_FileIO_EventQueue_ResultType.Baselib_FileIO_EventQueue_CloseFile = nil