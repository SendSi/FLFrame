---@class Unity.Jobs.LowLevel.Unsafe.JobProducerTypeAttribute : System.Attribute
Unity.Jobs.LowLevel.Unsafe.JobProducerTypeAttribute = {}

---@property readonly Unity.Jobs.LowLevel.Unsafe.JobProducerTypeAttribute.ProducerType : System.Type
Unity.Jobs.LowLevel.Unsafe.JobProducerTypeAttribute.ProducerType = nil

---@param producerType : System.Type
---@return Unity.Jobs.LowLevel.Unsafe.JobProducerTypeAttribute
function Unity.Jobs.LowLevel.Unsafe.JobProducerTypeAttribute(producerType)
end