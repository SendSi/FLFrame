---@class FairyGUI_UIObjectFactory_GLoaderCreator_Event : LuaInterface.LuaDelegate
FairyGUI_UIObjectFactory_GLoaderCreator_Event = {}

---@param func : LuaInterface.LuaFunction
---@return FairyGUI_UIObjectFactory_GLoaderCreator_Event
function FairyGUI_UIObjectFactory_GLoaderCreator_Event(func)
end

---@param func : LuaInterface.LuaFunction
---@param self : LuaInterface.LuaTable
---@return FairyGUI_UIObjectFactory_GLoaderCreator_Event
function FairyGUI_UIObjectFactory_GLoaderCreator_Event(func, self)
end

---@return FairyGUI.GLoader
function FairyGUI_UIObjectFactory_GLoaderCreator_Event:Call()
end

---@return FairyGUI.GLoader
function FairyGUI_UIObjectFactory_GLoaderCreator_Event:CallWithSelf()
end