---@class LuaInterface_LuaMethodWrap : System.Object
LuaInterface_LuaMethodWrap = {}

---@return LuaInterface_LuaMethodWrap
function LuaInterface_LuaMethodWrap()
end

---@param L : LuaInterface.LuaState
function LuaInterface_LuaMethodWrap.Register(L)
end