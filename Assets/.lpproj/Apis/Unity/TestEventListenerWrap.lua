---@class TestEventListenerWrap : System.Object
TestEventListenerWrap = {}

---@return TestEventListenerWrap
function TestEventListenerWrap()
end

---@param L : LuaInterface.LuaState
function TestEventListenerWrap.Register(L)
end