---@class UnityEngine_Application_AdvertisingIdentifierCallback_Event : LuaInterface.LuaDelegate
UnityEngine_Application_AdvertisingIdentifierCallback_Event = {}

---@param func : LuaInterface.LuaFunction
---@return UnityEngine_Application_AdvertisingIdentifierCallback_Event
function UnityEngine_Application_AdvertisingIdentifierCallback_Event(func)
end

---@param func : LuaInterface.LuaFunction
---@param self : LuaInterface.LuaTable
---@return UnityEngine_Application_AdvertisingIdentifierCallback_Event
function UnityEngine_Application_AdvertisingIdentifierCallback_Event(func, self)
end

---@param param0 : System.String
---@param param1 : System.Boolean
---@param param2 : System.String
function UnityEngine_Application_AdvertisingIdentifierCallback_Event:Call(param0, param1, param2)
end

---@param param0 : System.String
---@param param1 : System.Boolean
---@param param2 : System.String
function UnityEngine_Application_AdvertisingIdentifierCallback_Event:CallWithSelf(param0, param1, param2)
end