---@class UnityEngine_TrackedReferenceWrap : System.Object
UnityEngine_TrackedReferenceWrap = {}

---@return UnityEngine_TrackedReferenceWrap
function UnityEngine_TrackedReferenceWrap()
end

---@param L : LuaInterface.LuaState
function UnityEngine_TrackedReferenceWrap.Register(L)
end