---@class UnityEngine_AudioClip_PCMReaderCallback_Event : LuaInterface.LuaDelegate
UnityEngine_AudioClip_PCMReaderCallback_Event = {}

---@param func : LuaInterface.LuaFunction
---@return UnityEngine_AudioClip_PCMReaderCallback_Event
function UnityEngine_AudioClip_PCMReaderCallback_Event(func)
end

---@param func : LuaInterface.LuaFunction
---@param self : LuaInterface.LuaTable
---@return UnityEngine_AudioClip_PCMReaderCallback_Event
function UnityEngine_AudioClip_PCMReaderCallback_Event(func, self)
end

---@param param0 : System.Single[]
function UnityEngine_AudioClip_PCMReaderCallback_Event:Call(param0)
end

---@param param0 : System.Single[]
function UnityEngine_AudioClip_PCMReaderCallback_Event:CallWithSelf(param0)
end