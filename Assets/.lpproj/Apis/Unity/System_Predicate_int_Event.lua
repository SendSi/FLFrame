---@class System_Predicate_int_Event : LuaInterface.LuaDelegate
System_Predicate_int_Event = {}

---@param func : LuaInterface.LuaFunction
---@return System_Predicate_int_Event
function System_Predicate_int_Event(func)
end

---@param func : LuaInterface.LuaFunction
---@param self : LuaInterface.LuaTable
---@return System_Predicate_int_Event
function System_Predicate_int_Event(func, self)
end

---@param param0 : System.Int32
---@return System.Boolean
function System_Predicate_int_Event:Call(param0)
end

---@param param0 : System.Int32
---@return System.Boolean
function System_Predicate_int_Event:CallWithSelf(param0)
end