---@class _LogType : System.Enum
_LogType = {}

---@field public _LogType.value__ : System.Int32
_LogType.value__ = nil

---@field public _LogType.Assert : _LogType
_LogType.Assert = nil

---@field public _LogType.Error : _LogType
_LogType.Error = nil

---@field public _LogType.Exception : _LogType
_LogType.Exception = nil

---@field public _LogType.Log : _LogType
_LogType.Log = nil

---@field public _LogType.Warning : _LogType
_LogType.Warning = nil