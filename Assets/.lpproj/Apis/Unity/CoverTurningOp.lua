---@class CoverTurningOp : System.Enum
CoverTurningOp = {}

---@field public CoverTurningOp.value__ : System.Int32
CoverTurningOp.value__ = nil

---@field public CoverTurningOp.None : CoverTurningOp
CoverTurningOp.None = nil

---@field public CoverTurningOp.ShowFront : CoverTurningOp
CoverTurningOp.ShowFront = nil

---@field public CoverTurningOp.HideFront : CoverTurningOp
CoverTurningOp.HideFront = nil

---@field public CoverTurningOp.ShowBack : CoverTurningOp
CoverTurningOp.ShowBack = nil

---@field public CoverTurningOp.HideBack : CoverTurningOp
CoverTurningOp.HideBack = nil