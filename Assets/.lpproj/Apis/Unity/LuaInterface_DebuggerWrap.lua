---@class LuaInterface_DebuggerWrap : System.Object
LuaInterface_DebuggerWrap = {}

---@return LuaInterface_DebuggerWrap
function LuaInterface_DebuggerWrap()
end

---@param L : LuaInterface.LuaState
function LuaInterface_DebuggerWrap.Register(L)
end