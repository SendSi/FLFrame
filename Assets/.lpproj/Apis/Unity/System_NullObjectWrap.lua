---@class System_NullObjectWrap : System.Object
System_NullObjectWrap = {}

---@return System_NullObjectWrap
function System_NullObjectWrap()
end

---@param L : LuaInterface.LuaState
function System_NullObjectWrap.Register(L)
end