---@module FastScriptReload.Examples
FastScriptReload.Examples = {}

---@class FastScriptReload.Examples.FunctionLibrary : UnityEngine.MonoBehaviour
FastScriptReload.Examples.FunctionLibrary = {}

---@return FastScriptReload.Examples.FunctionLibrary
function FastScriptReload.Examples.FunctionLibrary()
end

---@param name : FastScriptReload.Examples.FunctionName
---@return FastScriptReload.Examples.Function
function FastScriptReload.Examples.FunctionLibrary.GetFunction(name)
end

---@param u : System.Single
---@param v : System.Single
---@param t : System.Single
---@return UnityEngine.Vector3
function FastScriptReload.Examples.FunctionLibrary.Wave(u, v, t)
end

---@param u : System.Single
---@param v : System.Single
---@param t : System.Single
---@return UnityEngine.Vector3
function FastScriptReload.Examples.FunctionLibrary.MultiWave(u, v, t)
end

---@param u : System.Single
---@param v : System.Single
---@param t : System.Single
---@return UnityEngine.Vector3
function FastScriptReload.Examples.FunctionLibrary.Ripple(u, v, t)
end

---@param u : System.Single
---@param v : System.Single
---@param t : System.Single
---@return UnityEngine.Vector3
function FastScriptReload.Examples.FunctionLibrary.Sphere(u, v, t)
end

---@param u : System.Single
---@param v : System.Single
---@param t : System.Single
---@return UnityEngine.Vector3
function FastScriptReload.Examples.FunctionLibrary.Torus(u, v, t)
end