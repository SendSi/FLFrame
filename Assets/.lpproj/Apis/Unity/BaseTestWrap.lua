---@class BaseTestWrap : System.Object
BaseTestWrap = {}

---@return BaseTestWrap
function BaseTestWrap()
end

---@param L : LuaInterface.LuaState
function BaseTestWrap.Register(L)
end