---@class JetBrains.Annotations.NoEnumerationAttribute : System.Attribute
JetBrains.Annotations.NoEnumerationAttribute = {}

---@return JetBrains.Annotations.NoEnumerationAttribute
function JetBrains.Annotations.NoEnumerationAttribute()
end