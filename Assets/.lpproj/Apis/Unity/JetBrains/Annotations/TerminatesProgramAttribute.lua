---@class JetBrains.Annotations.TerminatesProgramAttribute : System.Attribute
JetBrains.Annotations.TerminatesProgramAttribute = {}

---@return JetBrains.Annotations.TerminatesProgramAttribute
function JetBrains.Annotations.TerminatesProgramAttribute()
end