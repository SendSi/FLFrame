---@class JetBrains.Annotations.ValueProviderAttribute : System.Attribute
JetBrains.Annotations.ValueProviderAttribute = {}

---@property readonly JetBrains.Annotations.ValueProviderAttribute.Name : System.String
JetBrains.Annotations.ValueProviderAttribute.Name = nil

---@param name : System.String
---@return JetBrains.Annotations.ValueProviderAttribute
function JetBrains.Annotations.ValueProviderAttribute(name)
end