---@class FairyGUI_EventCallback1_Event : LuaInterface.LuaDelegate
FairyGUI_EventCallback1_Event = {}

---@param func : LuaInterface.LuaFunction
---@return FairyGUI_EventCallback1_Event
function FairyGUI_EventCallback1_Event(func)
end

---@param func : LuaInterface.LuaFunction
---@param self : LuaInterface.LuaTable
---@return FairyGUI_EventCallback1_Event
function FairyGUI_EventCallback1_Event(func, self)
end

---@param param0 : FairyGUI.EventContext
function FairyGUI_EventCallback1_Event:Call(param0)
end

---@param param0 : FairyGUI.EventContext
function FairyGUI_EventCallback1_Event:CallWithSelf(param0)
end