---@class PathTools : System.Object
PathTools = {}

---@field public PathTools.AB_RESOURCES : System.String
PathTools.AB_RESOURCES = nil

---@return PathTools
function PathTools()
end

---@return System.String
function PathTools.GetABResoucesPath()
end

---@return System.String
function PathTools.GetABOutPath()
end

---@return System.String
function PathTools.GetPlatformName()
end

---@return System.String
function PathTools.GetPlatformPath()
end

---@return System.String
function PathTools.GetWWWPath()
end